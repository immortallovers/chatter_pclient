package com.example.chatter.tcpController;

public class ExponentialBackoff {

    private int RETRY_COUNT;
    private long MIN_WAIT_TIME;

    public ExponentialBackoff(long minWaitTime) {
        RETRY_COUNT = 0;
        MIN_WAIT_TIME = minWaitTime;
    }

    public long getWaitTime() {
        long waitTIme = ((long) Math.pow(2, RETRY_COUNT) * MIN_WAIT_TIME);
        RETRY_COUNT++;
        return waitTIme;
    }

    public void setRETRY_COUNT() {
        RETRY_COUNT = 0;
    }

}
