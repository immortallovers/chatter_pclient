package com.example.chatter.backGroundService;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.ActivityManager;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.R;
import com.example.chatter.chatActivity;
import com.example.chatter.chatModel.UserType;
import com.example.chatter.fileManager.FileManager;
import com.example.chatter.mediaManager.MediaManager;
import com.example.chatter.messageModel.MessageObject;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.securityUtilities.SecurityUtilities;
import com.example.chatter.tcpController.ExponentialBackoff;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ListeningThread implements Runnable {

    private TCPClient client;
    private static FileManager manager;
    private static Context context;
    private static final String EXTRA_USERNAME = "com.example.chatter.EXTRA_USERNAME";
    private static final String EXTRA_ISAGROUP= "com.example.chatter.EXTRA_ISAGROUP";
    private static final String MESSAGE_TO_CHATACTIVITY = "MESSAGE";
    private static final String MESSAGE_TO_AUTHACTIVITY = "C_ID_ANSW";
    private static final String MESSAGE_TO_CONTACTLIST = "CNTC_ANSW";
    private static final String MESSAGE_TO_GROUPRECEIVER = "GRPC_ANSW";
    private static final String MESSAGE_TO_GROUPUSERARCTIVITY = "GROUPUSER";
    private static final int NOTIFICATION_ID = 1234;
    private boolean stopService = false;
    private AccountManager aManager;
    private DownloadManager downloadManager;
    /*List containing the IDs of active donwloads*/
    private static List<Long> dManagerIDs;
    /*Maps the download ID to the file sender (user or user_group)*/
    private static Map<Long, String> iDsToSender;
    /*Used for the keepalive mechanism with the server*/
    private int TIMEOUT_CONT;
    private int MAX_TIMEOUT = 4;
    private String uName;
    private static long[] VIBRATION_PATTERN = {100, 250, 200, 250};
    /*Used to set an exponential backoff to connection retries - Waiting max 5 minutes*/
    private ExponentialBackoff backoff;
    private long MIN_WAIT_TIME = 10000L;
    private int MAX_RETRIES = 6;
    private int retry_count;

    public ListeningThread(Context context) {
        super();
        this.context = context;
    }

    @Override
    public void run() {

        client = TCPClient.getTCPClient();
        manager = FileManager.getIstanceFileManager();
        downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        dManagerIDs = new ArrayList<Long>();
        iDsToSender = new HashMap<Long, String>();
        String rcvd;
        long delay;

        backoff = new ExponentialBackoff(MIN_WAIT_TIME);
        retry_count = 0;

        while (!stopService) {

            while (!client.connect().equals("true")) {
                delay = backoff.getWaitTime();
                retry_count++;
                if (retry_count == MAX_RETRIES) {
                    retry_count = 0;
                    backoff.setRETRY_COUNT();
                }
                Log.d(LOGCostants.LOG_D_TAG, "TRYING TO CONNECT TO SERVER - WAIT_TIME " + delay/1000 + "s");
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            retry_count = 0;
            backoff.setRETRY_COUNT();

            TIMEOUT_CONT = 0;

            aManager = AccountManager.get(context);
            String accountType = context.getString(R.string.accountType);
            Account[] accountsList = aManager.getAccountsByType(accountType);
            if (accountsList.length>0) {
                String psw = aManager.getPassword(accountsList[0]);
                uName = accountsList[0].name;
                byte[] toBytes = SecurityUtilities.msgToSend(uName, psw, MessageUtilities.AUTH);
                if (toBytes != null) {
                    if (client.sendMsg(toBytes))
                        Log.d(LOGCostants.LOG_D_TAG, "SENT AUTH");
                }
                else {
                    Log.e(LOGCostants.LOG_E_TAG, "ERROR CREATING THE byte[] TO SEND AUTH");
                }
            }
            else {
                Log.d(LOGCostants.LOG_D_TAG, "NO ACCOUNT FOUND");
            }


            Log.d(LOGCostants.LOG_D_TAG, "LISTENING FOR SERVER MESSAGES");

            while ((TIMEOUT_CONT < MAX_TIMEOUT) && !stopService) {
                rcvd = client.rcvMsg();
                if (rcvd == null) {
                    TIMEOUT_CONT++;
                    continue;
                }

                Log.e(LOGCostants.LOG_E_TAG, "RCVD = " + rcvd);
                String[] parsed = MessageParser.messageParser(rcvd);
                switch (parsed[0]) {
                    case "KEEP":
                        TIMEOUT_CONT = 0;
                        break;
                    case "C_ID":
                        sendMessage("C_ID", rcvd);
                        if (parsed[1].equals("OK")) {
                            client.setSignedIn(true);
                        }
                        Log.d(LOGCostants.LOG_D_TAG, "BSERVICE : C_ID");
                        break;
                    case "AUTH":
                        if (parsed[1].equals("NO")) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
                                aManager.removeAccountExplicitly(accountsList[0]);
                            }
                            if (!appIsRunning()) {
                                stopService = true;
                                client.disconnect();
                            }
                            Log.d(LOGCostants.LOG_D_TAG, "BSERVICE : AUTH NO");
                        }
                        else if (parsed[1].equals("OK")){
                            Log.d(LOGCostants.LOG_D_TAG, "BSERVICE : AUTH OK");
                            client.setSignedIn(true);
                        }
                        else {
                            Log.e(LOGCostants.LOG_E_TAG, "BSERVICE : WRONG AUTH");
                        }
                        break;
                    case "CNTC":
                        sendMessage("CONTACT", rcvd);
                        break;
                    case "GRPC":
                        if (parsed[1].equals("NW")) {
                            String fileName = parsed[2] + "_" + "group";
                            if (!manager.createNewFile(fileName, context))
                                Log.e(LOGCostants.LOG_E_TAG, "FILE NON CREATO");
                        }
                        sendMessage("NEW_GROUP", rcvd);
                        break;
                    case "GRPU":
                        if (parsed[1].equals("OK")) {
                            MessageObject obj = new MessageObject();
                            obj.setType(UserType.OTHER);
                            String toWrite = context.getString(R.string.newUserGroupMsg) + " " + parsed[3];
                            obj.setMsg(toWrite);
                            obj.setGroupNAme(parsed[2]);
                            obj.setDate(new GregorianCalendar());
                            Log.d(LOGCostants.LOG_D_TAG, "GROUPUSER " + parsed[2]);
                            if (!manager.writeMsgObj(obj, context)) {
                                Log.e(LOGCostants.LOG_E_TAG, "MSG NON SCRITTO");
                            }
                            sendMessage("CHAT", rcvd);
                        }
                        sendMessage("GROUPUSER", rcvd);
                        break;
                    case "GRPA":
                        String fileName = parsed[1] + "_" + "group";
                        if (!manager.createNewFile(fileName, context))
                            Log.e(LOGCostants.LOG_E_TAG, "FILE NON CREATO");
                        sendMessage("NEW_GROUP", rcvd);
                        createNotification("NEW_GROUP", parsed[1], null, true);
                        break;
                    case "FILE":
                        if (parsed[1].equals("OK")) {
                            Log.e(LOGCostants.LOG_E_TAG, "FILE OK");
                            MessageObject img = new MessageObject();
                            img.setType(UserType.OTHER);
                            String[] groups = MessageParser.usernameParser(parsed[2]);
                            if (groups.length > 1) {
                                /*This is a message for a group*/
                                img.setGroupNAme(groups[1]);
                            } else {
                                img.setUsername(parsed[2]);
                            }
                            img.setMsg(parsed[3]);
                            img.setUsername(parsed[2]);
                            img.setIsAMediaFile(true);
                            img.setDate(new GregorianCalendar());
                            if (!manager.writeMsgObj(img, context)) {
                                Log.e(LOGCostants.LOG_E_TAG, "MSG NON SCRITTO -IMG");
                            }
                        }
                        sendMessage("CHAT", rcvd);
                        break;
                    case "NFLE":
                        String[] group = MessageParser.usernameParser(parsed[1]);
                        String name;
                        if (group.length > 1) {
                            name = group[0];
                        } else {
                            name = parsed[1];
                        }
                        /*If name == uName, do nothing - the user sent the file contained in this message*/
                        if (!name.equals(uName)) {
                            if (!parsed[2].equals("TIMEOUT")) {
                                PackageManager packageManager = context.getPackageManager();
                                int status = packageManager.checkPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE, context.getPackageName());
                                if (status == PackageManager.PERMISSION_GRANTED) {
                                    long id = MediaManager.downloadMediaFile(parsed[2], downloadManager);
                                    dManagerIDs.add(id);
                                    iDsToSender.put(id, parsed[1]);
                                }
                            } else {
                                /*The user was sent an expired file*/
                                sendFileMsg(rcvd, false);
                                sendMessage("CHAT", rcvd);
                            }
                        }
                        break;
                    case "TX_M":
                        Log.d(LOGCostants.LOG_D_TAG, "RECEIVED TX");
                        MessageObject obj = new MessageObject();
                        obj.setType(UserType.OTHER);
                        String[] groups = MessageParser.usernameParser(parsed[1]);
                        if (groups.length>1) {
                            /*This is a message for a group*/
                            obj.setGroupNAme(groups[1]);
                        }
                        else {
                            obj.setUsername(parsed[1]);
                        }
                        obj.setMsg(parsed[4]);
                        GregorianCalendar time = MessageParser.dateParser(parsed[2]);
                        if (time != null)
                            obj.setDate(MessageParser.dateParser(parsed[2]));
                        else
                            obj.setDate(new GregorianCalendar());
                        obj.setUsername(parsed[1]);
                        if (!manager.writeMsgObj(obj, context)) {
                            Log.e(LOGCostants.LOG_E_TAG, "MSG NON SCRITTO");
                        }
                        sendMessage("CHAT", rcvd);
                        break;
                    case "RX_M":
                        Log.d(LOGCostants.LOG_D_TAG, "RECEIVED RX");
                        Log.d(LOGCostants.LOG_D_TAG, rcvd);
                        MessageObject rx = new MessageObject();
                        rx.setType(UserType.SELF);
                        String[] g = MessageParser.usernameParser(parsed[1]);
                        if (g.length>1) {
                            /*This is a message for a group*/
                            rx.setGroupNAme(g[1]);
                            rx.setUsername(g[0]);
                            rx.setMsg(g[0] + ": " + parsed[4]);
                            if (!appIsRunning())
                                createNotification("CHAT", g[1], g[0] + ": " + parsed[4], true);
                        }
                        else {
                            rx.setUsername(parsed[1]);
                            rx.setMsg(parsed[4]);
                            if (!appIsRunning())
                                createNotification("CHAT", parsed[1], parsed[4], false);
                        }
                        rx.setDate(MessageParser.dateParser(parsed[2]));
                        if (!manager.writeMsgObj(rx, context)){
                            Log.e(LOGCostants.LOG_E_TAG, "MSG NON SCRITTO");
                        }
                        sendMessage("CHAT", rcvd);

                }
            }



           /*Connection could be reset. Setting the status of the connection as not signedIn*/
            client.disconnect();
            client.setSignedIn(false);
        }

        /*If we're out of the while, the service needs to stop*/
        Log.d(LOGCostants.LOG_D_TAG, "LISTENINGTHREAD - THREAD STOPPED");
    }


    /*Choosing the correct Receiver*/
    private static void sendMessage(String receiver, String msg) {
        String label;
        Intent intent = null;
        switch (receiver)  {
            case "C_ID":
                //Send the message to the AuthenticationActivity
                intent = new Intent(context.getString(R.string.authReceiver));
                intent.putExtra(MESSAGE_TO_AUTHACTIVITY, msg);
                break;
            case "CHAT":
                //Send the message to the ChatActivity
                intent = new Intent(context.getString(R.string.messageReceiver));
                intent.putExtra(MESSAGE_TO_CHATACTIVITY, msg);
                break;
            case "CONTACT":
                //Send the message to the ContactListActivity
                intent = new Intent(context.getString(R.string.contactReceiver));
                intent.putExtra(MESSAGE_TO_CONTACTLIST, msg);
                break;
            case "NEW_GROUP":
                //Send the message to the ChatListActivity
                intent = new Intent(context.getString(R.string.groupReceiver));
                intent.putExtra(MESSAGE_TO_GROUPRECEIVER, msg);
                break;
            case "GROUPUSER":
                //Send the message to the ContactListGroupActivity
                intent = new Intent(context.getString(R.string.groupUserReceiver));
                intent.putExtra(MESSAGE_TO_GROUPUSERARCTIVITY, msg);
        }
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }


    /*Checking that the app is running*/
    private static boolean appIsRunning() {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> runningTaskInfo = manager.getRunningTasks(1);
        ComponentName componentInfo = runningTaskInfo.get(0).topActivity;
        if(componentInfo.getPackageName().toString().equalsIgnoreCase(context.getPackageName().toString()))
            return true;
        return false;
    }


    /*Creates a notification*/
    private static void createNotification(String mode, String chat, String msg, boolean isAGroup) {
        NotificationCompat.Builder notification = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_launcher)
                .setContentTitle(chat)
                .setAutoCancel(true)
                .setCategory(Notification.CATEGORY_MESSAGE)
                .setVisibility(Notification.VISIBILITY_PUBLIC);

        notification.setVibrate(VIBRATION_PATTERN);
        notification.setLights(Color.RED, 1000, 5000);
        Uri defaultAlarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notification.setSound(defaultAlarmSound);

        switch (mode) {
            case "CHAT":
                notification.setContentText(msg);
                break;
            case "NEW_GROUP":
                notification.setContentText(context.getString(R.string.notificationNewGroup));
        }

        Intent intent = new Intent(context, chatActivity.class);
        intent.putExtra(EXTRA_USERNAME, chat);
        intent.putExtra(EXTRA_ISAGROUP, isAGroup);

        /*
         * The stack builder object will contain an artificial back stack for the started chatActivity.
         * This ensures that navigating backward from the chatActivity leads out of the app to the Home screen.
         */
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(chatActivity.class);
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, notification.build());
    }


    /*used by the NewFileReceiver to write the new file on the chat*/
    private static void sendFileMsg(String msg, boolean succeed) {
        MessageObject obj = new MessageObject();
        obj.setType(UserType.SELF);
        String[] parsed = MessageParser.messageParser(msg);
        String[] g = MessageParser.usernameParser(parsed[1]);
        if (g.length>1) {
            /*This is a message for a group*/
            obj.setGroupNAme(g[1]);
            obj.setUsername(g[0]);
            if (succeed) {
                obj.setMsg(g[0] + ": " + parsed[2]);
                obj.setIsAMediaFile(true);
                if (!appIsRunning())
                    createNotification("CHAT", g[1], g[0] + ": " + parsed[2], true);
            } else {
                if (parsed[2].equals("KO"))
                    obj.setMsg(g[0] + ": " + context.getString(R.string.unsuccessfulDownload));
                else
                    obj.setMsg(g[0] + ": " + context.getString(R.string.downloadTimeout));
            }
        }
        else {
            obj.setUsername(parsed[1]);
            if (succeed) {
                obj.setMsg(parsed[2]);
                obj.setIsAMediaFile(true);
                if (!appIsRunning())
                    createNotification("CHAT", parsed[1], parsed[2], false);
            } else {
                if (parsed[2].equals("KO"))
                    obj.setMsg(context.getString(R.string.unsuccessfulDownload));
                else
                    obj.setMsg(context.getString(R.string.downloadTimeout));
            }
        }
        obj.setDate(new GregorianCalendar());
        if (!manager.writeMsgObj(obj, context)){
            Log.e(LOGCostants.LOG_E_TAG, "MSG NON SCRITTO");
        }

    }

    /*Custom BroadcastReceiver to receive notifications when a file download was completed*/
    public static class NewFileReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                DownloadManager.Query dQuery = new DownloadManager.Query();
                dQuery.setFilterById(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0));
                DownloadManager manager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
                Cursor cursor = manager.query(dQuery);
                String sender = iDsToSender.get(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,0));
                if (cursor.moveToFirst()) {
                    if (cursor.getCount() > 0) {
                        int status = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
                        String msg;
                        if (status == DownloadManager.STATUS_SUCCESSFUL) {
                            String file = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_FILENAME));
                            String[] fileName = file.split("[/]");
                            msg = MessageUtilities.NFLE + "|" + sender + "|" + fileName[fileName.length-1];
                            sendFileMsg(msg, true);
                        } else {
                            int message = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_REASON));
                            msg = MessageUtilities.NFLE + "|" + sender + "|KO";
                            Log.e(LOGCostants.LOG_E_TAG, "Error downloading file from server: " + message);
                            sendFileMsg(msg, false);
                        }
                        sendMessage("CHAT", msg);
                    }
                }
                iDsToSender.remove(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,0));
                dManagerIDs.remove(intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID,0));
            }
        }
    }


}
