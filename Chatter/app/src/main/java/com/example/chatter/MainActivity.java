package com.example.chatter;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.example.chatter.backGroundService.BackgroundService;

public class MainActivity extends AppCompatActivity {


    private AccountManager aManager;
    private String accountType;
    private ImageView logo;
    private long delay = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        logo = (ImageView) findViewById(R.id.ChatterView);


        /*Start BackgroundService - should be already running*/
        Intent i = new Intent(this, BackgroundService.class);
        startService(i);


        /*Looking for an existing account to call Authenticator's getAuhtToken()*/
        accountType = getString(R.string.accountType);
        aManager = AccountManager.get(this);

        final Account[] accountsList = aManager.getAccountsByType(accountType);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if (accountsList.length > 1) {
                    Log.e(LOGCostants.LOG_E_TAG, "PIÙ DI UN ACCOUNT");
                }
                else if (accountsList.length == 0){
                    startActivity(new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT));
                    finish();
                }
                else {
                    Intent intent = new Intent(getApplicationContext(), chatListActivity.class);
                    startActivity(intent);
                    finish();
                }
            }

        }, delay);
    }


}
