package com.example.chatter.messageModel;

import com.example.chatter.chatModel.UserType;

import java.io.Serializable;
import java.util.GregorianCalendar;

public class MessageObject implements Serializable {

    private String msg;
    /*Used only in group chat*/
    private String groupNAme;
    private String username;
    private GregorianCalendar date;
    /*SELF if msg is sent by the user, OTHER if it's received*/
    private UserType type;
    private boolean isAMediaFile;

    public MessageObject() {
        super();
        isAMediaFile = false;
    }

    public void setMsg(String message) {
        msg = message;
    }

    public String getMsg() {
        return msg;
    }

    public void setGroupNAme(String groupNAme) {
        this.groupNAme = groupNAme;
    }

    public String getGroupNAme() {
        return groupNAme;
    }

    public void setDate(GregorianCalendar date) {
        this.date = date;
    }

    public GregorianCalendar getDate() {
        return date;
    }

    public void setType(UserType type) {
        this.type = type;
    }

    public UserType getType() {
        return type;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public boolean IsAMediaFile() {
        return isAMediaFile;
    }

    public void setIsAMediaFile(boolean isAMediaFile) {
        this.isAMediaFile = isAMediaFile;
    }

}
