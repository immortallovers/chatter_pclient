package com.example.chatter;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.chatter.chatModel.ChatMessageModel;
import com.example.chatter.chatModel.MessageStatus;
import com.example.chatter.chatModel.UserType;
import com.example.chatter.fileManager.FileManager;
import com.example.chatter.mediaManager.MediaManager;
import com.example.chatter.messageModel.MessageObject;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.securityUtilities.SecurityUtilities;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class chatActivity extends AppCompatActivity {

    private static final String EXTRA_USERNAME = "com.example.chatter.EXTRA_USERNAME";
    private static final String EXTRA_ISAGROUP= "com.example.chatter.EXTRA_ISAGROUP";
    private static final String EXTRA_GROUP = "com.example.chatter.EXTRA_GROUP";
    /*Used to take pictures (takePicture())*/
    private static final int REQUEST_MEDIAFILE_CAPTURE = 1;
    /*Used to retrieve files from gallery*/
    private static final int SELECT_MEDIAFILE = 2;
    /*Used to ask for WRITE_EXTERNAL_STORAGE permission*/
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    /*Might be a group name*/
    private String userName;
    private ListView chat_list_view;
    private ChatListAdapter chatListAdapter;
    private ArrayList<ChatMessageModel> chatMessageList;
    private EditText chat_edit_text;
    private ImageView  enter_chat;
    private TCPClient client;
    private MessageReceiver receiver;
    /*Number of messages to display when the activity is open*/
    private int maxMsg = 10;
    /*True if this is a group chat*/
    private boolean isAgroup;
    private Uri fileUri;
    /*True if the message i is an image*/
    private List<Boolean> isAMediaFile;
    /*Used by onPermissionResult to decide which action ("CAMERA" or "VIDEO") must be taken*/
    private String mediaMode;
    /*Max media file size allowed*/
    private int MAX_MEDIA_SIZE = 20971520;



    /*Listener for the send button of the chat_edit_text*/
    private EditText.OnKeyListener chatEditTextKeyListener = new View.OnKeyListener() {
        @Override
        public boolean onKey(View view, int i, KeyEvent keyEvent) {

            /*Send button pushed*/
            if ((keyEvent.getAction() == KeyEvent.ACTION_DOWN) && (i == KeyEvent.KEYCODE_ENTER)) {
                EditText tmp = (EditText) view;

                if (view == chat_edit_text) {
                    sendMessage(tmp.getText().toString(), UserType.OTHER, false, null, null, false);
                }

                chat_edit_text.setText("");
                return true;
            }
            return false;
        }
    };

    /*TextWatcher used for chat_edit_text*/
    private final TextWatcher chatEditTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (chat_edit_text.getText().toString().equals("")) {

            }
            else {
                enter_chat.setImageResource(R.drawable.input_send);
            }
        }

        /*Called every time something is changed*/
        @Override
        public void afterTextChanged(Editable editable) {
            if (editable.length() ==  0) {
                enter_chat.setImageResource(R.drawable.input_send);
            }
            else {
                enter_chat.setImageResource(R.drawable.input_send);
            }
        }
    };

    /*Sending a message when enter_chat is clicked*/
    private void sendMessage(String message, UserType type, boolean isAgroup, String sender, GregorianCalendar time, boolean isAFile) {

        if (message.trim().length() == 0) {
            /*void message*/
            return;
        }

        ChatMessageModel msg;

        if (type == UserType.OTHER) {

            /*
             * Preparing the byte[] to send to the server
             * Using UTF-8 encoding means each character is 8 bit long
             */
            byte[] encryptedMsg = SecurityUtilities.encryptMessage("Trunksistor12345", message);
            String send = userName + "|" + String.valueOf(encryptedMsg.length) +  "|";
            byte[] toBytes = send.getBytes(Charset.forName("UTF-8"));
            byte[] msgToSend = new byte [toBytes.length + encryptedMsg.length];
            System.arraycopy(toBytes, 0, msgToSend, 0, toBytes.length);
            System.arraycopy(encryptedMsg, 0, msgToSend, toBytes.length, encryptedMsg.length);
            boolean sent = client.sendMsg(msgToSend);

            Log.d(LOGCostants.LOG_D_TAG, "SENT MSG " + sent);

            if (sent) {
                /*creating a new message to display*/
                msg = new ChatMessageModel(message, type, MessageStatus.NOTSENT);
                msg.setTime(new Date().getTime());
                chatMessageList.add(msg);
                isAMediaFile.add(isAFile);

                if (chatListAdapter != null) {
                    chatListAdapter.notifyDataSetChanged();
                }
            }
            else {
                Toast.makeText(this, R.string.msgNotSent, Toast.LENGTH_SHORT).show();
            }
        }

        else {
            /*creating a new message to display*/
            String s;
            if (isAgroup) {
                s = sender + ": " + message;
            }
            else {
                s = message;
            }
            msg = new ChatMessageModel(s, type, MessageStatus.SENT);
            msg.setTime(time.getTime().getTime());
            chatMessageList.add(msg);
            isAMediaFile.add(isAFile);

            if (chatListAdapter != null) {
                chatListAdapter.notifyDataSetChanged();
            }
        }


    }

    private ImageView.OnClickListener enterChatListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (view == enter_chat) {
                sendMessage(chat_edit_text.getText().toString(), UserType.OTHER, false, null, null, false);
            }
            chat_edit_text.setText("");
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        /*Setting the Toolbar*/
        Toolbar chatToolbar = (Toolbar) findViewById(R.id.chat_toolbar);
        setSupportActionBar(chatToolbar);

        /*Getting username String*/
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            userName = extras.getString(EXTRA_USERNAME);
            isAgroup = extras.getBoolean(EXTRA_ISAGROUP);
        }

        /*Setting activity title as the user's name*/
        this.setTitle(userName);

        /*Setting the background*/
        getWindow().setBackgroundDrawable(getResources().getDrawable(R.drawable.background));


        /*Sending a message  --->  chat_edit_text - enter_chat*/
        chat_edit_text = (EditText) findViewById(R.id.chat_edit_text);
        chatMessageList = new ArrayList<ChatMessageModel>();
        chatListAdapter = new ChatListAdapter(chatMessageList, this);
        chat_edit_text.setOnKeyListener(chatEditTextKeyListener);
        enter_chat = (ImageView) findViewById(R.id.enter_chat);
        enter_chat.setOnClickListener(enterChatListener);
        chat_edit_text.addTextChangedListener(chatEditTextWatcher);



         /*chat_list_view*/
        chat_list_view = (ListView) findViewById(R.id.chat_list_view);
        chat_list_view.setAdapter(chatListAdapter);

         /*Setting a listener for the listView*/
        chat_list_view.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                ChatMessageModel mess = (ChatMessageModel) chatListAdapter.getItem(i);

                if (mess != null && isAMediaFile.get(i)) {
                    String file;
                    if (!isAgroup  || (isAgroup && mess.getUserType().equals(UserType.OTHER)))
                        file = mess.getMessage();
                    else
                        file = mess.getMessage().split("[:]")[1].trim();

                    Log.d(LOGCostants.LOG_D_TAG, "FILEPATH =" + file);

                   /*We need to show the selected file*/
                    String extension = MediaManager.getExtension(file);
                    String path;
                    if (MediaManager.belongsToAppDirectory(file)) {
                        path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/chatter/" + file;
                    }
                    else {
                        path = "/storage/emulated/0/DCIM/Camera/" + file;
                    }
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    String type;
                    if (extension.equals("jpg")) {
                        type = "image/*";
                    }
                    else if (extension.equals("mp4")) {
                        type = "video/*";
                    }
                    else {
                        type = "*/*";
                    }
                    intent.setDataAndType(Uri.fromFile(new File(path)), type);
                    startActivity(intent);

                }
            }
        });


        /*MessageReceiver*/
        receiver = new MessageReceiver();


        NotificationCenter.getInstance().addObserver(this, NotificationCenter.emojiDidLoaded);


    }

    /*Inflates menu on the Toolbar if this is a group chat*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if (isAgroup) {
            inflater.inflate(R.menu.chat_group_menu, menu);
        }
        else {
            inflater.inflate(R.menu.chat_menu, menu);
        }
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_user_to_chat:
                 /* Used to add a new user to the group chat */
                Intent intent = new Intent(getApplicationContext(), contactListGroupActivity.class);
                intent.putExtra(EXTRA_GROUP, userName);
                startActivity(intent);
                return true;
            case R.id.camera:
                permissionCheck("CAMERA");
                mediaMode = "CAMERA";
                return true;
            case R.id.video:
                permissionCheck("VIDEO");
                mediaMode = "VIDEO";
                return true;
            case R.id.attach:
                selectMediaFile();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        * Registering the MessageReceiver
        * onResume is called after onCreate and when recovering from onPause
        * */
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(getString(R.string.messageReceiver)));

        /*Updating the listView of the messages*/
        FileManager manager = FileManager.getIstanceFileManager();
        String file = userName;
        if (isAgroup) {
            file  += "_" + "group";
        }
        List<MessageObject> msgList = manager.readMsgObj(file, this);
        Log.d(LOGCostants.LOG_D_TAG, "CHAT SIZE " + msgList.size() + " username " + userName);
        int i;
        isAMediaFile = new ArrayList<Boolean>();
        if (msgList.size() > maxMsg) {
            for (i = msgList.size()- (maxMsg + 1); i < msgList.size(); i++) {
                MessageObject tmp = msgList.get(i);
                isAMediaFile.add(tmp.IsAMediaFile());
                ChatMessageModel msg = new ChatMessageModel(tmp.getMsg(), tmp.getType(), MessageStatus.SENT);
                msg.setTime(tmp.getDate().getTime().getTime());
                chatMessageList.add(msg);
            }
        }
        else {
            for (MessageObject tmp : msgList) {
                isAMediaFile.add(tmp.IsAMediaFile());
                ChatMessageModel msg = new ChatMessageModel(tmp.getMsg(), tmp.getType(), MessageStatus.SENT);
                msg.setTime(tmp.getDate().getTime().getTime());
                chatMessageList.add(msg);
            }

        }
        chatListAdapter.notifyDataSetChanged();
        /*Updating the TCPClient*/
        client = TCPClient.getTCPClient();
    }

    @Override
    protected void onPause() {
        super.onPause();

        /*Unregister the MessageReceiver if the activity is not running*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);

        /*We need to clean the list of messages which will be populated on onResume*/
        chatMessageList.clear();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



    /*The function open the default device app to retrieve files, if there's one*/
    private void selectMediaFile() {
        fileUri = null;
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("*/*");
        startActivityForResult(intent, SELECT_MEDIAFILE);
    }

    /*
     * The function open the deafult device app to take pictures or videos, if there's one
     * @mode = CAMERA or @mode = VIDEO
     * */
    private void takeMediaFile(String mode) {
        fileUri = null;
        Intent intent = null;
        if (mode.equals("CAMERA")) {
            intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        else if (mode.equals("VIDEO")) {
            intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
        }
        if (intent != null && intent.resolveActivity(getPackageManager()) != null) {
            /*There's a device app that can handle intent*/
            /*Creating the new file for the picture*/
            File mediaFile = null;
            try {
                mediaFile = MediaManager.createMediaFile(mode);
            } catch (IOException io) {
                Log.e(LOGCostants.LOG_E_TAG, "Errore nella creazione del file - chatActivity.takeMediaFile()");
                Toast.makeText(this, R.string.noFile, Toast.LENGTH_SHORT).show();
            }
            if (mediaFile != null) {
                /*File successfully created*/
                fileUri = FileProvider.getUriForFile(this, "com.example.android.fileprovider", mediaFile);
                intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(intent, REQUEST_MEDIAFILE_CAPTURE);
            }
        }
        else {
            Toast.makeText(this, R.string.noCamera, Toast.LENGTH_SHORT).show();
        }

    }

    /*Returns an intent with the taken/selected media file*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        boolean selected = false;

        if ((requestCode == REQUEST_MEDIAFILE_CAPTURE || requestCode == SELECT_MEDIAFILE) && resultCode == RESULT_OK) {
            if (requestCode == REQUEST_MEDIAFILE_CAPTURE) {
                MediaManager.addMediaFileToGallery(fileUri, this);
            }
            else if (requestCode == SELECT_MEDIAFILE) {
                fileUri = data.getData();
                selected = true;
            }
            sendMediaFile(selected, data);
        }
    }

    private void  sendMediaFile(final boolean selected, final Intent data) {

        final String[] fileName = new String[1];
        new AsyncTask<Void, Void, Boolean>() {

            @Override
            protected Boolean doInBackground(Void... params) {
                Cursor returnCursor = getContentResolver().query(fileUri, null, null, null, null);
                int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
                returnCursor.moveToFirst();
                int size = (int) returnCursor.getLong(sizeIndex);
                Log.d(LOGCostants.LOG_D_TAG, "SIZE FILE " + size);
                /*If the file is bigger than 20MB do not send it*/
                if (size > MAX_MEDIA_SIZE)
                    return false;

                sizeIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                fileName[0] = (String) returnCursor.getString(sizeIndex);
                String send = MessageUtilities.FILE + "|" + userName + "|" + fileName[0] + "|" + Integer.toString(size) + "|";
                Log.d(LOGCostants.LOG_D_TAG, "FILE " + send);

                /*Getting the byte[] to send*/
                byte[] toBytes = send.getBytes(Charset.forName("UTF-8"));
                if (!client.sendMsg(toBytes)) {
                    return false;
                }

                /*Reading the file*/
                byte[] bytes = MediaManager.getFileBytes(fileName[0], chatActivity.this, fileUri, selected, data);
                if (bytes == null) {
                    Log.e(LOGCostants.LOG_E_TAG, "Bytes NULL");
                    return false;
                }

                /*Sending the file*/
                return client.sendFile(bytes);

            }

            @Override
            protected void onPostExecute(Boolean result) {

                if (result) {
                    ChatMessageModel msg = new ChatMessageModel(fileName[0], UserType.OTHER, MessageStatus.NOTSENT);
                    msg.setTime(new Date().getTime());
                    chatMessageList.add(msg);
                    isAMediaFile.add(true);
                    chatListAdapter.notifyDataSetChanged();
                }
                else {
                    Toast.makeText(chatActivity.this, R.string.fileNotSent, Toast.LENGTH_SHORT).show();
                }

            }
        }.execute();
    }

    /*Function used to explicitly ask user for permissions (WRITE_EXTERNAL_STORAGE)*/
    private void permissionCheck(String mode) {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
        }
        if (!addPermission(permissionsList, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("READ_EXTERNAL_STORAGE");
        }

        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need grant to access to " + permissionsNeeded.get(0);
                for (int i = 0; i < permissionsNeeded.size(); i++) {
                    message += ", " + permissionsNeeded.get(i);
                    showMessage(message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_PERMISSIONS);
                            }
                        }
                    });
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }
        takeMediaFile(mode);
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return false;
            }
        }
        return true;
    }

    /*Used to show a message to explain user why the app needs the permission when user first denied it*/
    private void showMessage(String msg, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /*Always called after a permission check to return result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.READ_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i=0; i<permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                if ((perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) && (perms.get(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    // Permission Granted
                    takeMediaFile(mediaMode);
                }else {
                    // Permission Denied
                    Toast.makeText(this, "WRITE_EXTERNAL_STORAGE denied - Can't take pictures/videos", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }


    /*Get the index of tha message msg in the chatMessageList - msg was always sent from the user*/
    private int getMessageIndex(String msg){
        if (chatMessageList.size()>0) {
            for (int i = 0; i < chatMessageList.size(); i++) {
                if (chatMessageList.get(i).getMessage().equals(msg) && chatMessageList.get(i).getMessageStatus().equals(MessageStatus.NOTSENT))
                    return i;
            }
        }
        return -1;
    }


    /*Custom BroadcastReceiver to read messages from BackgroundService when the activity is running*/
    public class MessageReceiver extends BroadcastReceiver {

        private final String MESSAGE_TO_CHATACTIVITY = "MESSAGE";

        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(MESSAGE_TO_CHATACTIVITY);
            String[] parsed = MessageParser.messageParser(msg);

            if (parsed[0].equals(MessageUtilities.MOD_RX)) {
                /*Received a msg*/
                String[] isAGroup = MessageParser.usernameParser(parsed[1]);
                if (isAGroup.length > 1 && isAGroup[1].equals(userName)) {
                    /*This is a message for a group*/
                    sendMessage(parsed[4], UserType.SELF, true, isAGroup[0], MessageParser.dateParser(parsed[2]),false);
                } else if (parsed[1].equals(userName)) {
                    sendMessage(parsed[4], UserType.SELF, false, null,  MessageParser.dateParser(parsed[2]),false);
                }
            } else if (parsed[0].equals(MessageUtilities.MOD_TX)) {
                /*A msg sent from the user was successfully received by server*/
                Log.d(LOGCostants.LOG_D_TAG, "CHAT TX " + msg);
                String[] isAGroup = MessageParser.usernameParser(parsed[1]);
                if ((isAGroup.length > 1 && isAGroup[1].equals(userName)) || parsed[1].equals(userName)) {
                    /*Getting the index of the msg in the chat list (if it's contained in the list), -1 otherwise*/
                    int index = getMessageIndex(parsed[4]);
                    Log.d(LOGCostants.LOG_D_TAG, "CHAT TX INDEX " + index);
                    if (index != -1) {
                        chatMessageList.get(index).setTime(MessageParser.dateParser(parsed[2]).getTime().getTime());
                        chatMessageList.get(index).setMessageStatus(MessageStatus.SENT);
                        chatListAdapter.notifyDataSetChanged();
                    }
                }
            } else if (parsed[0].equals(MessageUtilities.GRPU)) {
                /*Adding new user to group chat*/
                if (parsed[2].equals(userName)) {
                    String tmp = context.getString(R.string.newUserGroupMsg) + " " + parsed[3];
                    sendMessage(tmp, UserType.OTHER, false, null, null,false);
                }
            } else if (parsed[0].equals(MessageUtilities.FILE)) {
                /*File sent from the user was succssfully received by the server*/
                String[] isAGroup = MessageParser.usernameParser(parsed[2]);
                String name;
                if (isAGroup.length>1)
                    name = isAGroup[1];
                else
                    name = parsed[2];
                if (name.equals(userName)) {
                    if (parsed[1].equals("OK")) {
                        int index = getMessageIndex(parsed[3]);
                        if (index != -1) {
                            chatMessageList.get(index).setMessageStatus(MessageStatus.SENT);
                            chatListAdapter.notifyDataSetChanged();
                            Toast.makeText(chatActivity.this, R.string.fileSent, Toast.LENGTH_SHORT).show();
                        }
                    } else if (parsed[1].equals("NO")) {
                        Toast.makeText(chatActivity.this, R.string.fileNotSent, Toast.LENGTH_SHORT).show();
                    }
                }
            } else if (parsed[0].equals(MessageUtilities.NFLE)) {
                /*Receiving a new file*/
                String message;
                boolean media = false;
                if (parsed[2].equals("KO")) {
                    /*The download of the file was not successful*/
                    message = getString(R.string.unsuccessfulDownload);
                } else if (parsed[2].equals("TIMEOUT")) {
                    /*Received an expired file*/
                    message = getString(R.string.downloadTimeout);
                } else {
                    /*The download of the file was successful*/
                    message = parsed[2];
                    media = true;
                }
                String[] isAGroup = MessageParser.usernameParser(parsed[1]);
                if (isAGroup.length > 1 && isAGroup[1].equals(userName)) {
                        /*This is a message for a group*/
                    sendMessage(message, UserType.SELF, true, isAGroup[0], new GregorianCalendar(), media);
                } else if (parsed[1].equals(userName)) {
                    sendMessage(message, UserType.SELF, false, null,  new GregorianCalendar(), media);
                }
            }
        }
    }
}


