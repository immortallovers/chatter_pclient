package com.example.chatter.parser;

import android.util.Log;

import com.example.chatter.LOGCostants;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;


/*SINGLETON CLASS*/
public class MessageParser {

    /*Maps the number of pipes expected to its command*/
    private Map<String, Integer> cmdToPipe;
    private static MessageParser parser;


    private MessageParser() {
        cmdToPipe = new HashMap<String, Integer>();
        populateHashMap();
    }

    private void populateHashMap() {
        cmdToPipe.put("AUTH", 2);
        cmdToPipe.put("C_ID", 2);
        cmdToPipe.put("CNTC", 3);
        cmdToPipe.put("GRPC", 3);
        cmdToPipe.put("GRPU", 4);
        cmdToPipe.put("GRPA", 3);
        cmdToPipe.put("FILE", 4);
        cmdToPipe.put("NFLE", 3);
        cmdToPipe.put("TX_M", 4);
        cmdToPipe.put("RX_M", 4);
    }



    /*Returns the only istance of this class*/
    public static MessageParser getMessageParser() {
        if (parser == null) {
            parser = new MessageParser();
        }
        return parser;
    }

    /*Returns the number of pipes expected for the specific command*/
    public int getNumOfPipes(String cmd) {
        return cmdToPipe.get(cmd);
    }

    /*Parsing the string received from the server*/
    public static String[] messageParser(String rcvd) {
        String delims = "[|]";
        String[] msg = rcvd.split(delims);
        return msg;
    }

    /*Creating the date*/
    public static GregorianCalendar dateParser(String date) {
        String delims = "[:]";
        String[] parsed = date.split(delims);
        Log.d(LOGCostants.LOG_D_TAG, "DATE " + date + "DATE SIZE " + parsed.length);
        if (parsed.length == 6) {
            return new GregorianCalendar(Integer.parseInt(parsed[0]), Integer.parseInt(parsed[1]) - 1, Integer.parseInt(parsed[2]), Integer.parseInt(parsed[3]), Integer.parseInt(parsed[4]), Integer.parseInt(parsed[5]));
        }
        else
            return null;
    }

    /*Returns the username of the sender user and the name of the group if the message belongs to a group, rcvd otherwise*/
    public static String[] usernameParser(String rcvd) {
        String delims = "[_]";
        return rcvd.split(delims);
    }
}
