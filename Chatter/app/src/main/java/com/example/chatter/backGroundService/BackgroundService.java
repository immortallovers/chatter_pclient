package com.example.chatter.backGroundService;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.fileManager.FileManager;
import com.example.chatter.messageModel.MessageObject;
import com.example.chatter.tcpController.TCPClient;

public class BackgroundService extends Service {

    public class LocalBinder extends Binder {
        BackgroundService getService() {
            return BackgroundService.this;
        }
    }

    private final IBinder binder = new LocalBinder();
    private Thread lThread;
    private ListeningThread listeningThread;

    @Override
    public void onCreate() {
        super.onCreate();
        listeningThread = new ListeningThread(this);
        lThread = new Thread(listeningThread);
        lThread.start();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(LOGCostants.LOG_D_TAG, "SERVICE STARTED");
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }
}
