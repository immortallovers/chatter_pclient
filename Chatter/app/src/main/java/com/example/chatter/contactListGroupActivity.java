package com.example.chatter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.chatter.chatModel.UserType;
import com.example.chatter.contentProvider.ContactsProvider;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.nio.charset.Charset;
import java.util.List;

public class contactListGroupActivity extends AppCompatActivity{

    private static final String EXTRA_GROUP = "com.example.chatter.EXTRA_GROUP";
    private ListView contacts_list;
    private List<String> listOfContacts;
    private String userName;
    private String group;
    private Context context;
    private GroupUserReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactslist);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            group = extras.getString(EXTRA_GROUP);
        }

        context = this;
        receiver = new GroupUserReceiver();

        /*CONTACTS LIST*/
        contacts_list = (ListView) findViewById(R.id.contacts_list);
        listOfContacts = ContactsProvider.getUsernameList(this);
        final ArrayAdapter<String> conctactsListAdapter = new ArrayAdapter<String>(this, R.layout.listview_layout, android.R.id.text1, listOfContacts);
        contacts_list.setAdapter(conctactsListAdapter);
        contacts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                userName = conctactsListAdapter.getItem(i).toString();
                String send = MessageUtilities.GRPU + "|" + group + "|" + userName + "|";
                 /*Getting the byte[] to send*/
                byte[] toBytes = send.getBytes(Charset.forName("UTF-8"));

                if (!TCPClient.getTCPClient().sendMsg(toBytes)) {
                    Toast.makeText(context, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
         /*
        * Registering the GroupUserReceiver
        * onResume is called after onCreate and when recovering from onPause
        * */
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(getString(R.string.groupUserReceiver)));
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*Unregister the ContactReceiver if the activity is not running*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    /*Custom BroadcastReceiver to read messages from BackgroundService when the activity is running*/
    public class GroupUserReceiver extends BroadcastReceiver {

        private final String MESSAGE_TO_GROUPUSERARCTIVITY = "GROUPUSER";

        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(MESSAGE_TO_GROUPUSERARCTIVITY);
            String[] parsed = MessageParser.messageParser(msg);
            String answer = parsed[0] + "|" + parsed[1];
            if (answer.equals(MessageUtilities.GRPU_OK_ANSW)) {
                Toast.makeText(context, "Utente " + parsed[3] + " aggiunto al gruppo " + parsed[2], Toast.LENGTH_SHORT).show();
                finish();
            }
            else if(answer.equals(MessageUtilities.GRPU_NO_ANSW)) {
                Toast.makeText(context, R.string.noSuchContact, Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(context, R.string.notAdminGroup, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
