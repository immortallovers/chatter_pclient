package com.example.chatter.fileManager;

import android.content.Context;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.chatModel.UserType;
import com.example.chatter.messageModel.MessageObject;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


/*SINGLETON CLASS*/
public class FileManager {

    /*Maps username/group name to file descriptor*/
    private Map<String, File> mapUserToFile;
    /*Maps username/group name to lock*/
    private Map<String, Lock> mapUserToLock;
    private static FileManager manager;

    private FileManager() {
        mapUserToFile = new HashMap<>();
        mapUserToLock = new HashMap<>();
    }

    public static FileManager getIstanceFileManager() {
        if (manager == null) {
            manager = new FileManager();
        }
        return manager;
    }


    /*Writes a new message on tha chat file*/
    public boolean writeMsgObj(MessageObject obj, Context context) {
        File file;
        Lock lock;
        FileOutputStream foS = null;
        ObjectOutputStream objectOutputStream= null;
        boolean result = true;
        String username;

        if (obj.getGroupNAme() != null) {
            username = obj.getGroupNAme() + "_" + "group";;
        }
        else {
            username = obj.getUsername();
        }

        Log.d(LOGCostants.LOG_D_TAG, "WRITE " + username);

        if ((file = mapUserToFile.get(username)) == null) {
            /*There is no file descriptor for this user yet*/
            file = new File(context.getFilesDir(), username);
            Log.d(LOGCostants.LOG_D_TAG, "FILE " + file);
            mapUserToFile.put(username, file);
            lock = new ReentrantLock();
            mapUserToLock.put(username, lock);
        }
        else {
            lock = mapUserToLock.get(username);
        }


        Log.d(LOGCostants.LOG_D_TAG, "FILE " + username + " SIZE " + file.length());

        try {
            lock.lock();
            foS = new FileOutputStream(file, true);
            if (file.length()>0) {
                objectOutputStream = new ObjectOutputStream(foS) {
                    protected void writeStreamHeader() throws IOException {
                        reset();
                    }
                };
            }
            else {
                objectOutputStream = new ObjectOutputStream(foS);
            }
            objectOutputStream.writeObject(obj);
            Log.d(LOGCostants.LOG_D_TAG, "WRITTEN");
        } catch (IOException e) {
            result = false;
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }

        try {
            objectOutputStream.close();
            foS.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        Log.d(LOGCostants.LOG_D_TAG, "FILE " + username + " SIZE 2 " + file.length());

        return result;
    }

    /*Reads all messages for a chat*/
    public List<MessageObject> readMsgObj(String username, Context context) {
        List<MessageObject> objList = new ArrayList<MessageObject>();
        File file;
        Lock lock;
        FileInputStream fis = null;
        ObjectInputStream objectInputStream = null;

        if ((file = mapUserToFile.get(username)) == null) {
            file = new File(context.getFilesDir(), username);
            mapUserToFile.put(username, file);
            lock = new ReentrantLock();
            mapUserToLock.put(username, lock);
        }
        else {
            lock = mapUserToLock.get(username);
        }

        try {
            lock.lock();
            fis = new FileInputStream(file);
            objectInputStream = new ObjectInputStream(fis);
            while (true) {
                MessageObject tmp = (MessageObject) objectInputStream.readObject();
                objList.add(tmp);
            }
        } catch (FileNotFoundException f) {
            f.printStackTrace();
        } catch (EOFException eof) {
            eof.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            lock.unlock();
        }

        if (fis != null && objectInputStream != null) {
            try {
                objectInputStream.close();
                fis.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        return objList;
    }

    /*Returns a list containing the existing chats*/
    public List<String> getOpenChats(Context context) {
        List<String> list = new ArrayList<String>();
        File directory = context.getFilesDir();
        File[] files = directory.listFiles();

        for (int i=0; i<files.length; i++) {
            if (!files[i].getName().equals("lib"))
                list.add(files[i].getName());
        }

        return list;
    }

    /*Used to create a new file when the user joins a new group*/
    public boolean createNewFile(String groupName, Context context) {
        File file;
        Lock lock;
        boolean result;

        if ((file = mapUserToFile.get(groupName)) == null) {
            /*there's no file for this group*/
            file = new File(context.getFilesDir(), groupName);
            mapUserToFile.put(groupName, file);
            lock = new ReentrantLock();
            mapUserToLock.put(groupName, lock);

            try {
                lock.lock();
                result = file.createNewFile();
            } catch (IOException e) {
                result = false;
                e.printStackTrace();
            } finally {
                lock.unlock();
            }
        }
        result = true;
        return result;
    }

}
