package com.example.chatter.securityUtilities;

import android.util.Base64;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.tcpController.MessageUtilities;

import java.nio.charset.Charset;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public class SecurityUtilities {

    /*Number of bytes returned from the hashed password*/
    private static final int numberOfBytesHashed = 16;
    /*Number of bytes used for the cipher's secret key*/
    private static final int numberOfSecretKeyBytes = 16;
    /*Number of bytes used for the cipher's IV*/
    private static final int numberOfIVBytes = 16;


    /*Returns the first 16 char of the hashed password*/
    private static byte[] getSha256(String pswd) {
        MessageDigest msgDigest;

        try {
            msgDigest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            Log.e(LOGCostants.LOG_E_TAG, "NoSuchAlgorithmException - getSha256");
            return null;
        }
        msgDigest.update(pswd.getBytes(Charset.forName("UTF-8")));
        byte[] msgHashedBytes = msgDigest.digest();
        byte[] hashedPswd = new byte[numberOfBytesHashed];
        for (int i = 0; i < numberOfBytesHashed; i++) {
            hashedPswd[i] = msgHashedBytes[i];
        }
        return hashedPswd;
    }

    /*Returns the AUTH or C_ID byte[] to send to the server*/
    public static byte[] msgToSend(String username, String pswd, String mode) {

        byte[] hashedPswd = getSha256(pswd);
        if (hashedPswd != null) {
            byte[] msgBytes;

            if (mode.equals(MessageUtilities.AUTH)) {
                String send = MessageUtilities.AUTH + "|" + username + "|";
                msgBytes = send.getBytes(Charset.forName("UTF-8"));
            } else if (mode.equals(MessageUtilities.C_ID)) {
                String send = MessageUtilities.C_ID + "|" + username + "|";
                msgBytes = send.getBytes(Charset.forName("UTF-8"));
            } else {
                return null;
            }

             /*Getting a byte[] to contain the msgBytes + the hashedPswd + a byte for the final "|"*/
            byte[] toSend = new byte[msgBytes.length + hashedPswd.length + 1];
            int i;
            for (i = 0; i < msgBytes.length; i++) {
                toSend[i] = msgBytes[i];
            }
            int j = 0;
            for (i = msgBytes.length; i < msgBytes.length + hashedPswd.length; i++) {
                toSend[i] = hashedPswd[j];
                j++;
            }
            toSend[msgBytes.length + hashedPswd.length] = "|".getBytes(Charset.forName("UTF-8"))[0];
            return toSend;
        }
        return null;
    }


    /*
    * Returns a Cipher object to encrypt/decrypt the message
    * @mode = "ENCRYPT" or @mode = "DECRYPT"
    * */
    private static Cipher getCipher(String mode, String key) {
        SecretKeySpec secretKeySpec = getKey(key);
        Cipher cipher;
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Log.e(LOGCostants.LOG_E_TAG, "NoSuchAlgorithmException - getCipher");
            return null;
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
            Log.e(LOGCostants.LOG_E_TAG, "NoSuchPaddingException - getCipher");
            return null;
        }
        IvParameterSpec IV = getIV();
        try {
            if (mode.equals("ENCRYPT")) {
                cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec, IV);
            }
            else if (mode.equals("DECRYPT")) {
                cipher.init(Cipher.DECRYPT_MODE, secretKeySpec, IV);
            }
            else
                return null;
        } catch (InvalidKeyException e) {
            e.printStackTrace();
            Log.e(LOGCostants.LOG_E_TAG, "InvalidKeyException - getCipher");
            return null;
        } catch (InvalidAlgorithmParameterException e) {
            e.printStackTrace();
            Log.e(LOGCostants.LOG_E_TAG, "InvalidAlgorithmParameterException - getCipher");
            return null;
        }
        return cipher;
    }

    /*Returns the key for the cipher*/
    private static SecretKeySpec getKey(String key) {
        byte[] keybytes = new byte[numberOfSecretKeyBytes];
        byte[] getKeyBytes = key.getBytes(Charset.forName("UTF-8"));
        System.arraycopy(getKeyBytes, 0, keybytes, 0, Math.min(keybytes.length, getKeyBytes.length));
        return new SecretKeySpec(keybytes, "AES");
    }

    /*Returns the IV for the cipher*/
    private static IvParameterSpec getIV() {
        String IV = "ImmortalLovers67";
        byte[] IVBytes = IV.getBytes(Charset.forName("UTF-8"));
        return new IvParameterSpec(IVBytes);
    }

    /*Returns the byte[] containing the encrypted msg*/
    public static byte[] encryptMessage(String key, String msg) {
        Cipher cipher = getCipher("ENCRYPT", key);
        if (cipher != null) {
            try {
                return cipher.doFinal(msg.getBytes(Charset.forName("UTF-8")));
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "IllegalBlockSizeException - encryptMessage");
            } catch (BadPaddingException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "BadPaddingException - encryptMessage");
            }
        }
        return null;
    }


    /*Returns the decrypted msg*/
    public static String decryptMessage(String key, byte[] msg) {
        Cipher cipher = getCipher("DECRYPT", key);
        if (cipher != null) {
            try {
                byte[] decryptedMsg = cipher.doFinal(msg);
                return new String(decryptedMsg, 0, decryptedMsg.length);
            } catch (IllegalBlockSizeException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "IllegalBlockSizeException - decryptMessage");
            } catch (BadPaddingException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "BadPaddingException - decryptMessage");
            }
        }
        return null;
    }
}
