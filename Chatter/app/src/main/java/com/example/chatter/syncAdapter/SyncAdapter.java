package com.example.chatter.syncAdapter;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProvider;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SyncResult;
import android.os.Bundle;


/*SINGLETON CLASS - INSTANTIATED  IN THE SyncService*/
public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private ContentResolver resolver;

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        resolver = context.getContentResolver();
    }

    /*This form of the constructor maintains compatibility with older platform versions*/
    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        resolver = context.getContentResolver();
    }


     @Override
    public void onPerformSync(Account account, Bundle bundle, String s, ContentProviderClient contentProviderClient, SyncResult syncResult) {
        /* - Connecting to a server
         * - Downloading and uploading data
         * - Handling data conflicts or determining how current the data is
         * - Clean up (close connections, delete tmp files,...)
         */
    }
}
