package com.example.chatter.mediaManager;

import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import com.example.chatter.LOGCostants;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.jar.Manifest;

public class MediaManager {

    /*
     * Creates the new file (and, if needed, a new directory) for the picture
     * @mode = CAMERA or @mode = VIDEO
     * */
    public static File createMediaFile(String mode) throws IOException{
        //Create a unique image file name
        String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        File directory = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "chatter");
        if (!isExternalStorageWritable() || !directory.mkdir()) {
            Log.e(LOGCostants.LOG_E_TAG, "Impossibile creare directory chatter o directory già esistente- createImageFile");
        }

        File mediaFile = null;
        if (mode.equals("CAMERA")) {
            String fileName = "JPEG_" + timestamp + "_";
            mediaFile = File.createTempFile(fileName, ".jpg", directory);
        }
        else if (mode.equals("VIDEO")) {
            String fileName = "VIDEO_" + timestamp + "_";
            mediaFile = File.createTempFile(fileName, ".mp4", directory);
        }
        return mediaFile;

    }


    /*Checks if the external storage can be read/written*/
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /*Update the device gallery with the new file*/
    public static void addMediaFileToGallery(Uri mediaFile, Context context) {
        Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        intent.setData(mediaFile);
        context.sendBroadcast(intent);
    }

    /*Gets the whole file*/
    public static byte[] getFileBytes(String fileName, Context context, Uri fileUri, boolean selected, Intent data) {
        Cursor returnCursor = context.getContentResolver().query(fileUri, null, null, null, null);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        int size = (int) returnCursor.getLong(sizeIndex);

        String path;
        if (!selected) {
            path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/chatter/" + fileName;
        }
        else {
            path = getPathForSelectedFile(fileUri, context, data);
            Log.e(LOGCostants.LOG_E_TAG, "PATH " + path);
        }

        if (path != null) {
            File file = new File(path);


            byte[] bytes = new byte[size];
            FileInputStream inStream;

            try {
                inStream = new FileInputStream(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "FileNotFoundException - getFilesBytes");
                return null;
            }

            try {
                inStream.read(bytes);
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "Errore nella lettura del file - getFileBytes");
                return null;
            }

            try {
                inStream.close();
            } catch (IOException e) {
                e.printStackTrace();
                Log.e(LOGCostants.LOG_E_TAG, "Errore nella chiusura dello stream - getFilesBytes");
            }

            return bytes;
        }
        return null;
    }

    /*
     * Returns the file extension
     * JPEG or MP4
     * */
    public static String getExtension(String fileName) {
        String delims = "[.]";
        String[] parts = fileName.split(delims);
        return parts[parts.length -1];
    }

    /*Retrieving the correct path for the file selected from gallery*/
    private static String getPathForSelectedFile(Uri fileUri, Context context, Intent data) {
        String path = null;

        String id = fileUri.getLastPathSegment().split(":")[1];
        final String[] projection = {MediaStore.Images.Media.DATA };
        Uri uri = getUri();

        Cursor imageCursor = context.getContentResolver().query(uri, projection, MediaStore.Images.Media._ID + "=" +id, null, null);
        if (imageCursor.moveToFirst()) {
            path = imageCursor.getString(imageCursor.getColumnIndex(projection[0]));
        }

        return path;
    }

    private static Uri getUri() {
        String state = Environment.getExternalStorageState();
        if(!state.equalsIgnoreCase(Environment.MEDIA_MOUNTED))
            return MediaStore.Images.Media.INTERNAL_CONTENT_URI;
        return MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
    }


    /*Listing the app's directory files*/
    public static boolean belongsToAppDirectory(String fileName) {
        List<String> list = new ArrayList<String>();
        String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath() + "/chatter";
        File directory = new File(path);
        File[] files = directory.listFiles();

        for (int i = 0; i < files.length; i++) {
            if (files[i].getName().equals(fileName))
                return true;
        }

        return false;
    }


    /*Starts the download of a new media file from the server*/
    public static long downloadMediaFile(String url, DownloadManager downloadManager) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
        String[] parts = url.split("[/]");
        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_PICTURES, "/chatter/" + parts[parts.length-1]);
        return downloadManager.enqueue(request);
    }

}
