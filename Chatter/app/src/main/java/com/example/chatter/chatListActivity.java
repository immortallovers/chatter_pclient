package com.example.chatter;

import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatter.fileManager.FileManager;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class chatListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private ListView chat_list;
    private List<String> listOfChats;
    private ArrayAdapter<String> chatListAdapter;
    private static final String EXTRA_USERNAME = "com.example.chatter.EXTRA_USERNAME";
    private static final String EXTRA_ISAGROUP= "com.example.chatter.EXTRA_ISAGROUP";
    /*Used to ask for READ_CONTACTS permission*/
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;
    private Context context = this;
    private GroupReceiver receiver;
    private boolean[] isAGroup;
    private NavigationView navigationView;
    /*Used to tell the permissionCheck() if it needs ti open a contacListActivity or give permission to read profile info*/
    private boolean openContactList = false;
    private boolean readProfileInfo = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatlist);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*NAVIGATION DRAWER*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        permissionCheck();
        if (readProfileInfo)
            settingNavigationDrawer();


        receiver = new GroupReceiver();

        /*CHAT LIST*/
        listOfChats = new ArrayList<String>();
        List<String> files = FileManager.getIstanceFileManager().getOpenChats(this);
        /*We need to know if the the selected chat is a group or not, to set the toolbar*/
        isAGroup = populateIsAGroup(files);
        int i;
        for (i=0; i < files.size(); i++) {
            if (isAGroup[i] == true) {
                listOfChats.add(MessageParser.usernameParser(files.get(i))[0]);
            }
            else {
                listOfChats.add(files.get(i));
            }
        }

        /*Adding Adapter for the ListView (Opens selected user's chat)*/
        chat_list = (ListView) findViewById(R.id.chat_list);
        chatListAdapter = new ArrayAdapter<String>(this, R.layout.listview_layout, android.R.id.text1,listOfChats);
        chat_list.setAdapter(chatListAdapter);
        chat_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String username = chatListAdapter.getItem(i).toString();
                if (username != null) {
                    /* Creating a new Activity with the chat related to the selected user*/
                    Intent intent = new Intent(getApplicationContext(), chatActivity.class);
                    intent.putExtra(EXTRA_USERNAME, username);
                    if (isAGroup[i]) {
                        intent.putExtra(EXTRA_ISAGROUP, true);
                    }
                    else {
                        intent.putExtra(EXTRA_ISAGROUP, false);
                    }
                    startActivity(intent);
                }
                else {
                    /*It should never happen, but the user might delete the contact's username from the device's phoneBook thus leaving the app with an open chat but no valid username*/
                    Toast.makeText(context, R.string.noValidUsesrname, Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        /*
        * Registering the GroupReceiver
        * onResume is called after onCreate and when recovering from onPause
        * */
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(getString(R.string.groupReceiver)));

        openContactList = false;

        List<String> files = FileManager.getIstanceFileManager().getOpenChats(this);
        if (files.size() > listOfChats.size()) {
            int i = listOfChats.size();
            isAGroup = populateIsAGroup(files);
            while(i < files.size()) {
                if (isAGroup[i])
                    chatListAdapter.add(MessageParser.usernameParser(files.get(i))[0]);
                else
                    chatListAdapter.add(files.get(i));
                i++;
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*Unregister the GroupReceiver if the activity is not running*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        }
        else {
            super.onBackPressed();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_addGroup) {
            openDialogGroup();

        }
        else if (id == R.id.nav_add_contact) {
        /*
         * First we need to explicitly check if we're granted the permission to read the device's contact list from the user
         * If the permission is granted the function calls the related activity with the list of contacts, otherwise it shows a message error
         * */

            openContactList = true;
            permissionCheck();

        }
        else if (id == R.id.nav_signIn) {
            /*
             * We need to prevent the user from sending damaging sign up requests checking first if there is a saved account with credentials.
             * If there's an account, no actions should be performed.
             */
            AccountManager aManager = AccountManager.get(this);
            Account[] accountsList = aManager.getAccountsByType(this.getString(R.string.accountType));
            if (accountsList.length == 0) {
                //No account found
                startActivity(new Intent(android.provider.Settings.ACTION_ADD_ACCOUNT));
                finish();
            }
            else {
                Toast.makeText(context, R.string.existingAccount, Toast.LENGTH_SHORT).show();
            }

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    /*Sets the navigation drawer's fields (userName and profile picture)*/
    private void settingNavigationDrawer() {
        View navigationViewHeader = navigationView.getHeaderView(0);
        ImageView headerIconView = (ImageView) navigationViewHeader.findViewById(R.id.imageView);
        TextView headerTextView = (TextView) navigationViewHeader.findViewById(R.id.textViewTitle);
        Cursor profileCursor = getProfileCursor();

        if (profileCursor != null) {
            headerTextView.setText(profileCursor.getString(profileCursor.getColumnIndex(ContactsContract.Profile.DISPLAY_NAME_PRIMARY)));
            String uriString = profileCursor.getString(profileCursor.getColumnIndex(ContactsContract.Profile.PHOTO_THUMBNAIL_URI));
            Uri uri;
            if (uriString != null) {
                uri = Uri.parse(uriString);
            }
            else {
                uri = getGmailAccountPictureUri();
            }
            if (uri != null) {
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                    RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(getResources(), bitmap);
                    roundedBitmapDrawable.setCornerRadius(50.0f);
                    roundedBitmapDrawable.setAntiAlias(true);
                    headerIconView.setImageDrawable(roundedBitmapDrawable);
                } catch (IOException e) {
                    e.printStackTrace();
                    Log.e(LOGCostants.LOG_E_TAG, "IOException - setNavigationDrawer() - chatListActivity");
                }
            }
        }
    }

    /*Retrieves the cursor of device's user profile info*/
    private Cursor getProfileCursor() {
        String[] projection = new String[] {
                ContactsContract.Profile.DISPLAY_NAME_PRIMARY,
                ContactsContract.Profile.PHOTO_THUMBNAIL_URI,
                ContactsContract.Profile.IS_USER_PROFILE
        };

        Cursor cursor = getContentResolver().query(ContactsContract.Profile.CONTENT_URI, projection, null, null, null);
        if (cursor.getCount()>0) {
            if (cursor.moveToFirst()) {
                do {
                    if (cursor.getString(cursor.getColumnIndex(ContactsContract.Profile.IS_USER_PROFILE)).equals("1")) {
                        return cursor;
                    }
                } while (cursor.moveToNext());
            }
        }
        return null;

    }


    /*Returns the URI of the pictures beloning to the devices Google/Gmail account*/
    private Uri getGmailAccountPictureUri() {
        Uri pictureUri = null;

        Account[] accounts = AccountManager.get(this).getAccountsByType("com.google");
        Account account = null;
        if (accounts.length>0) {
            account = accounts[0];
        }

        if (account != null) {
            String[] projection = {
                    ContactsContract.CommonDataKinds.Email.DATA,
                    ContactsContract.CommonDataKinds.Email.PHOTO_URI
            };
            Cursor cursor = this.getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, projection,
                    ContactsContract.CommonDataKinds.Email.DATA + " = ?",
                    new String[]{account.name}, null);
            if (cursor.getCount()>0) {
                if (cursor.moveToFirst()) {
                    do {
                        pictureUri = Uri.parse(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.PHOTO_URI)));
                        break;
                    } while (cursor.moveToNext());
                }
            }

        }
        return pictureUri;
    }


    /*Returns an array of boolean values, if boolean[i]==true -> files.get(i) is a group chat*/
    private boolean[] populateIsAGroup(List<String> files) {
        boolean[]  isAGroup = new boolean[files.size()];
        int i;
        for (i=0; i < files.size(); i++) {
            String[] parts = MessageParser.usernameParser(files.get(i));
            if (parts.length > 1) {
                /*This is a group*/
                isAGroup[i] = true;
            }
            else {
                isAGroup[i] = false;
            }
        }

        return isAGroup;
    }


    /*Called by the GroupReceiver to update the array isAGroup -> the new chat is always a group*/
    private boolean[] updateIsAGroup() {
        boolean[]  isAGroup = new boolean[this.isAGroup.length + 1];
        for(int i=0; i<this.isAGroup.length; i++) {
            isAGroup[i] = this.isAGroup[i];
        }
        isAGroup[this.isAGroup.length] = true;
        return isAGroup;
    }


    /*Checks if the group name contains special chars*/
    private boolean containsSpecialChar(String groupName) {
        if (groupName.contains("_") || groupName.contains("|"))
            return true;
        return false;
    }


    /*Dialog used to insert the name of a new group*/
    private void openDialogGroup() {
        AlertDialog.Builder builder = new AlertDialog.Builder(chatListActivity.this);
        View v = chatListActivity.this.getLayoutInflater().inflate(R.layout.group_dialog_layout, null);
        builder.setView(v);
        final EditText groupName = (EditText) v.findViewById(R.id.dialogGroup);
        builder.setTitle(getString(R.string.dialogGTitle))
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                })
                .setPositiveButton("INSERT", new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        String title = groupName.getText().toString();
                        if (!title.equals("") && title.length()>4 && !containsSpecialChar(title)) {

                            title = title.toLowerCase();
                            String send = MessageUtilities.GRPC + "|" + title + "|";
                             /*Getting the byte[] to send*/
                            byte[] toBytes = send.getBytes(Charset.forName("UTF-8"));

                            if (!TCPClient.getTCPClient().sendMsg(toBytes)) {
                                Toast.makeText(context, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
                            }

                        }
                        else {
                            groupName.setText(R.string.dialogGroupTitle);
                            Toast.makeText(context, R.string.invalidGroupName, Toast.LENGTH_SHORT).show();
                        }
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


     /*Function used to explicitly ask user for permissions (READ_CONTACTS)*/
    private void permissionCheck() {
        List<String> permissionsNeeded = new ArrayList<String>();
        final List<String> permissionsList = new ArrayList<String>();

        if (!addPermission(permissionsList, Manifest.permission.READ_CONTACTS)) {
            permissionsNeeded.add("READ_CONTACTS");
        }
        if (!addPermission(permissionsList, Manifest.permission.WRITE_CONTACTS)) {
            permissionsNeeded.add("WRITE_CONTACTS");
        }
        if (!addPermission(permissionsList, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            permissionsNeeded.add("WRITE_EXTERNAL_STORAGE");
        }



        if (permissionsList.size() > 0) {
            if (permissionsNeeded.size() > 0) {
                String message = "You need grant to access to " + permissionsNeeded.get(0);
                for (int i = 0; i < permissionsNeeded.size(); i++) {
                    message += ", " + permissionsNeeded.get(i);
                    showMessage(message, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_PERMISSIONS);
                            }
                        }
                    });
                    return;
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permissionsList.toArray(new String[permissionsList.size()]), REQUEST_CODE_ASK_PERMISSIONS);
            }
            return;
        }
        if (openContactList)
            startActivity(new Intent(getApplicationContext(), contactsListActivity.class));
        else
            readProfileInfo = true;
    }

    private boolean addPermission(List<String> permissionsList, String permission) {
        if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
            permissionsList.add(permission);
            if (!ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                return false;
            }
        }
        return true;
    }

    /*Used to show a message to explain user why the app needs the permission when user first denied it*/
    private void showMessage(String msg, DialogInterface.OnClickListener listener) {
        new AlertDialog.Builder(this)
                .setMessage(msg)
                .setPositiveButton("OK", listener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    /*Always called after a permission check to return result*/
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS: {
                Map<String, Integer> perms = new HashMap<String, Integer>();
                perms.put(Manifest.permission.READ_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_CONTACTS, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);

                for (int i=0; i<permissions.length; i++) {
                    perms.put(permissions[i], grantResults[i]);
                }

                if ((perms.get(Manifest.permission.READ_CONTACTS) == PackageManager.PERMISSION_GRANTED) && (perms.get(Manifest.permission.WRITE_CONTACTS) == PackageManager.PERMISSION_GRANTED) && (perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)) {
                    // Permission Granted
                    if (openContactList)
                        startActivity(new Intent(getApplicationContext(), contactsListActivity.class));
                    else
                        readProfileInfo = true;
                }else {
                    // Permission Denied
                    Toast.makeText(this, "READ_CONTACTS denied - Can't show contacts list/profile info\n  WRITE_EXTERNAL_STORAGE denied - Can't download files", Toast.LENGTH_SHORT).show();
                }
                break;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /*Custom BroadcastReceiver to read messages from BackgroundService when the activity is running*/
    public class GroupReceiver extends BroadcastReceiver {

        private final String MESSAGE_TO_GROUPRECEIVER = "GRPC_ANSW";

        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(MESSAGE_TO_GROUPRECEIVER);
            String[] parsed = MessageParser.messageParser(msg);
            String ctrl = parsed[0] + "|" + parsed[1];
            if (ctrl.equals(MessageUtilities.GRPC_NW_ANSW)) {
                Toast.makeText(context, getString(R.string.groupApproved) + " -> " + parsed[2], Toast.LENGTH_SHORT).show();
                listOfChats.add(parsed[2]);
                isAGroup = updateIsAGroup();
                chatListAdapter.notifyDataSetChanged();
            } else if (ctrl.equals(MessageUtilities.GRPC_NO_ANSW)){
                Toast.makeText(context, R.string.usedGroupName, Toast.LENGTH_SHORT).show();
            } else if (parsed[0].equals(MessageUtilities.GRPA)) {
                listOfChats.add(parsed[1]);
                isAGroup = updateIsAGroup();
                chatListAdapter.notifyDataSetChanged();
            }
        }
    }

}
