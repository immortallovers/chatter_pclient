package com.example.chatter.syncAdapter;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.chatter.LOGCostants;

/**
 * Define a Service that returns an IBinder for the
 * sync adapter class, allowing the sync adapter framework to call
 * onPerformSync().
 */

public class SyncService extends Service {

    /*Singleton instance of the SyncAdapter*/
    private static SyncAdapter syncAdapter = null;
    // Object to use as a thread-safe lock
    private static final Object syncAdapterLock = new Object();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(LOGCostants.LOG_E_TAG, "SYNC_SERVICE");
        /*Set the syncAdapter as syncable, disallow parallel syncs - Thread-safe constructor*/
        synchronized (syncAdapterLock) {
            if (syncAdapter == null) {
                syncAdapter = new SyncAdapter(getApplicationContext(), true);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        /* Get the object that allows external processes to call onPerformSync(). The object is created in the base class code when the SyncAdapter constructors call super() */
        return syncAdapter.getSyncAdapterBinder();
    }
}
