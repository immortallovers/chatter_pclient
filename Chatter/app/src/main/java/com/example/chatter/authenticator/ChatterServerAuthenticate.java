package com.example.chatter.authenticator;

import android.content.res.Resources;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.R;
import com.example.chatter.chatModel.UserType;
import com.example.chatter.securityUtilities.SecurityUtilities;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.nio.charset.Charset;


public class ChatterServerAuthenticate {

    /*Manage the actual connection with the server to authenticate the client*/

    public String serverConnect(String username, String password, String mode) {
        String authToken = "true";


        TCPClient client = TCPClient.getTCPClient();

         /*Getting the byte[] to send*/
        byte[] toBytes = SecurityUtilities.msgToSend(username, password, MessageUtilities.C_ID);

        if (toBytes == null || !client.sendMsg(toBytes)) {
            authToken = "false";
        }

        return authToken;
    }

}
