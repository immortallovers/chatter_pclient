package com.example.chatter.broadcasterReceiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.backGroundService.BackgroundService;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Intent i = new Intent(context, BackgroundService.class);
        context.startService(i);
        Log.d(LOGCostants.LOG_D_TAG, "STARTED BackgroundService");
    }
}
