package com.example.chatter.authenticator;

import android.accounts.Account;
import android.accounts.AccountAuthenticatorActivity;
import android.accounts.AccountManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatter.LOGCostants;
import com.example.chatter.R;
import com.example.chatter.chatListActivity;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import static com.example.chatter.authenticator.AuthenticatorUtilities.serverAuthenticate;


public class AuthenticationActivity extends AccountAuthenticatorActivity {

    private AccountManager aManager;
    private Button signInButton;
    private String authTokenType;
    private String accountName;
    private String accountPswd;
    private TextView aName;
    private TextView aPswd;
    private int delay = 500;
    private Intent intent;
    private AuthReceiver receiver;

    /*signIn function*/
    private void signIn() {
        final String accountType = getIntent().getStringExtra(AuthenticatorUtilities.ARG_ACCOUNT_TYPE);
        signInButton.setEnabled(false);

        new AsyncTask<Object, Object, String>() {

            @Override
            protected String doInBackground(Object... voids) {

                String auth;
                Bundle b = new Bundle();

                //signin() should be called only when the user needs to create a new account on the server
                auth = serverAuthenticate.serverConnect(accountName, accountPswd, MessageUtilities.C_ID);
                Log.d(LOGCostants.LOG_D_TAG, "AUTH-SIGNIN " + auth);

                if (auth == "true") {

                    b.putString(AccountManager.KEY_ACCOUNT_NAME, accountName);
                    b.putString(AccountManager.KEY_ACCOUNT_TYPE, accountType);
                    b.putString(AccountManager.KEY_AUTHTOKEN, auth);
                    b.putString(AuthenticatorUtilities.ARG_ACCOUNT_PSWD, accountPswd);

                    intent = new Intent();
                    intent.putExtras(b);
                }
                return auth;
            }

            @Override
            protected void onPostExecute(String s) {
                if (s.equals("false")) {
                    Toast.makeText(AuthenticationActivity.this, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
                    signInButton.setEnabled(false);
                }
            }
        }.execute();

    }


    /*Called by the AuthReceiver*/
    private void completeLogin() {
        if (intent != null) {
            String auth = intent.getStringExtra(AccountManager.KEY_AUTHTOKEN);
            Log.e(LOGCostants.LOG_E_TAG, "AUTH " + auth);
            if (auth != null && auth.equals("true")) {
                /*
                 * The signin request has been sent successfully
                 * We need to wait for the server's ack
                 */

                String accountName = intent.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                String accountPswd = intent.getStringExtra(AuthenticatorUtilities.ARG_ACCOUNT_PSWD);
                String accountType = intent.getStringExtra(AccountManager.KEY_ACCOUNT_TYPE);

                final Account account = new Account(accountName, accountType);
                if (getIntent().getBooleanExtra(AuthenticatorUtilities.ARG_BOOL_ADDING_NEW_ACCOUNT, true)) {
                    /*We need to create a new account*/
                    aManager.addAccountExplicitly(account, accountPswd, null);
                    aManager.setAuthToken(account, authTokenType, auth);
                }
                setAccountAuthenticatorResult(intent.getExtras());
                setResult(RESULT_OK, intent);
                delayFinish();
                finish();
            }
            else if (auth.equals("false")){
                Toast.makeText(AuthenticationActivity.this, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
                delayFinish();
                finish();

            }
            else if (auth.equals("usedUname")) {
                Toast.makeText(AuthenticationActivity.this, R.string.usedUname, Toast.LENGTH_SHORT).show();
                aName.setText("");
            }
        }
        else {
            Toast.makeText(AuthenticationActivity.this, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
            delayFinish();
            finish();
        }

    }



    private void delayFinish() {
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                Intent i = new Intent(getApplicationContext(), chatListActivity.class);
                startActivity(i);
            }

        }, delay);
    }


    /*Checks if username or password contain special chars*/
    private boolean containsSpecialChar() {
        if (accountName.contains("_") || accountName.contains("|") || accountPswd.contains("_") || accountPswd.contains("|"))
            return true;
        return false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authentication);

        aName = (TextView) findViewById(R.id.accountName);
        aPswd = (TextView) findViewById(R.id.accountPassword);

        String userName = getIntent().getStringExtra(AuthenticatorUtilities.ARG_ACCOUNT_NAME);
        authTokenType = getIntent().getStringExtra(AuthenticatorUtilities.ARG_AUTH_TYPE);
        if (authTokenType == null) {
            authTokenType = AuthenticatorUtilities.AUTHTOKEN_TYPE_FULL_ACCESS;
        }

        aManager = AccountManager.get(getBaseContext());
        signInButton = (Button) findViewById(R.id.signIn);


        if (userName != null) {
            ((TextView) findViewById(R.id.accountName)).setText(userName);
        }

        /*Initializing the receiver from the BackgroundService*/
        receiver = new AuthReceiver();

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                accountName = aName.getText().toString();
                accountPswd = aPswd.getText().toString();
                if (accountName.equals("") || accountPswd.equals("")) {
                    aName.setText("");
                    aPswd.setText("");
                    Toast.makeText(AuthenticationActivity.this, R.string.invalidToast, Toast.LENGTH_SHORT).show();
                } else if (accountName.length() < 4 || accountPswd.length() < 5) {
                    aName.setText("");
                    aPswd.setText("");
                    Toast.makeText(AuthenticationActivity.this, R.string.notEnoughChar, Toast.LENGTH_SHORT).show();
                } else if (containsSpecialChar()) {
                    aName.setText("");
                    aPswd.setText("");
                    Toast.makeText(AuthenticationActivity.this, R.string.specialChar, Toast.LENGTH_SHORT).show();
                } else {
                    Log.d(LOGCostants.LOG_D_TAG, "BUTTON SIGN IN");
                    accountName = accountName.toLowerCase();
                    signIn();
                }
            }
        });

    }



    @Override
    public void onResume() {
        super.onResume();
        /*
        * Registering the AuthReceiver
        * onResume is called after onCreate and when recovering from onPause
        * */
        if (!TCPClient.getTCPClient().getSignedIn()) {
            LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(getString(R.string.authReceiver)));
        }
        else {
            completeLogin();
            Toast.makeText(this, getString(R.string.authReceived), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        /*Unregister the MessageReceiver if the activity is not running*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /*Custom BroadcastReceiver to read messages from BackgroundService when the activity is running*/
    public class AuthReceiver extends BroadcastReceiver {

        private final String MESSAGE_TO_AUTHACTIVITY = "C_ID_ANSW";

        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(MESSAGE_TO_AUTHACTIVITY);

            if (msg.equals(MessageUtilities.C_ID_OK_ANSW)) {
                completeLogin();
                Toast.makeText(context, getString(R.string.authReceived), Toast.LENGTH_SHORT).show();
            }
            else {
                signInButton.setEnabled(true);
                Toast.makeText(context, getString(R.string.usedUname), Toast.LENGTH_SHORT).show();
            }
        }
    }

}
