package com.example.chatter.contentProvider;


public class ContactObject {

    private String rawContactID;
    private String userName;

    public ContactObject() {
        super();
        rawContactID = null;
        userName = null;
    }

    public void setRawContactID(String id) {
        rawContactID = id;
    }

    public String getRawContactID() {
        return rawContactID;
    }

    public void setUserName(String uName) {
        userName = uName;
    }

    public String getUserName() {
        return userName;
    }

}
