package com.example.chatter.tcpController;

import android.provider.MediaStore;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.securityUtilities.SecurityUtilities;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import static com.example.chatter.parser.MessageParser.getMessageParser;

/**
 * Created by cristina on 23/11/16.
 */


/*SINGLETON CLASS*/
public class TCPClient {

    private static TCPClient client;

    private static Socket clientS;
    //private static String serverURL = "chatter.ml";
    private static String serverIP = "192.168.1.5";
    private static int serverPort =10000;
    private InetSocketAddress serverAddr = null;
    private int CONNECT_TIMEOUT = 1000;
    private int READ_TIMEOUT = 35000;
    //used to send messages
    private DataOutputStream send;
    //used to received messages ("\n" means end of message)
    private DataInputStream rcvd;
    /*used to check that the client is connected AND signed in to the server*/
    private boolean signedIn;
    /*used to acces signedIn atomically*/
    private Lock slock;
    /*used to send a message*/
    private Lock sendLock;
    /*max number of bytes that the client can send to the server in a single chunck*/
    private double chunkSize = 4000.0;
    private int cmdChar = 4;



    private TCPClient() {
        signedIn = false;
        slock = new ReentrantLock();
        sendLock = new ReentrantLock();
    }

    /*Returns the only existing instance of TCPClient class*/
    public static TCPClient getTCPClient() {
        if (client == null) {
            client = new TCPClient();
        }
        return client;
    }

    /*
    * Connecting to the server
    * Returns "true" if the connection is established without errors, "false" otherwise
    * */
    public String connect() {
        try {
            //InetAddress url = InetAddress.getByName(serverURL);
            serverAddr = new InetSocketAddress(serverIP, serverPort);
            clientS = new Socket();
            clientS.setSoTimeout(READ_TIMEOUT);
            //Imposto un timeout di 1s
            clientS.connect(serverAddr, CONNECT_TIMEOUT);
        } catch(SocketException s) {
            Log.e(LOGCostants.LOG_E_TAG, "SocketException trying to set the socket Timeout - TCPClient");
            s.printStackTrace();
            return "false";
        } catch (IOException u) {
            Log.e(LOGCostants.LOG_E_TAG, "IOException trying to set the InetAddress serverAddr - TCPClient");
            u.printStackTrace();
            return "false";
        }

        try {
            send = new DataOutputStream(clientS.getOutputStream());
            rcvd = new DataInputStream(clientS.getInputStream());
        }
        catch (IOException e) {
            Log.e(LOGCostants.LOG_E_TAG, "IOException trying to establish a connection with the server - TCPClient");
            return "false";
        }
        return "true";
    }


    /*Disconnect from the server*/
    public void disconnect() {
        slock.lock();
        if (signedIn) {
             try {
                 send.close();
                 rcvd.close();
                 clientS.close();
              } catch (IOException e) {
                 Log.e(LOGCostants.LOG_E_TAG, "IOException trying to close the connection - TCPClient");
              }
        }
        slock.unlock();
    }

    /*
    * Sending a msg to the server
    * */
    public boolean sendMsg(byte[] msg) {
        boolean success = true;
        if (send != null) {
            slock.lock();
            sendLock.lock();
            try {
                send.write(msg);
                send.flush();
            } catch (IOException e) {
                success = false;
                e.printStackTrace();
            } finally {
                slock.unlock();
                sendLock.unlock();
            }
            return success;
        }
        return false;
    }

    /* Sending a file to the server */
    public boolean sendFile(byte[] file) {
        boolean success = true;
        int numChunks = (int) Math.ceil(file.length/chunkSize);

        /*Preparing last chunk*/
        byte[] finalChunk = new byte[file.length - ((int)chunkSize*(numChunks-1))];
        for (int x=0; x< finalChunk.length; x++) {
            finalChunk[x] = file[((int)chunkSize*(numChunks-1)) + x];
        }

        if (send!= null) {
            sendLock.lock();
            slock.lock();

            if (signedIn) {

                /*Sending the file*/
                int i;
                for (i = 0; i < numChunks; i++) {
                    if (i != numChunks - 1) {
                        try {
                            send.write(file, (i * (int) chunkSize), (int) chunkSize);
                            send.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                            success = false;
                        } finally {
                            if (!success) {
                                Log.e(LOGCostants.LOG_E_TAG, "Errore nell'invio del chunk " + i + " su " + numChunks);
                                sendLock.unlock();
                                slock.unlock();
                                return success;
                            }
                        }
                    } else {
                        try {
                            send.write(finalChunk);
                            send.flush();
                        } catch (IOException e) {
                            e.printStackTrace();
                            Log.e(LOGCostants.LOG_E_TAG, "Errore nell'inviod dell'ultimo chunk ");
                            success = false;
                        }finally {
                            if (!success) {
                                sendLock.unlock();
                                slock.unlock();
                                return success;
                            }
                        }
                    }
                }
            }
        }
        sendLock.unlock();
        slock.unlock();
        return success;
    }


    /*
    * Receiving a msg from the server
    * */
    public String rcvMsg() {
        String tmp = "";
        MessageParser parser = getMessageParser();
        int cont = 0;
        String msg = "";

        if (rcvd != null) {

            try {
                byte[] buf = new byte[1];

                int bytesRead = 0;

                /*Reading the command first*/
                while (cont < cmdChar) {
                    if ((bytesRead = rcvd.read(buf)) != 0 && bytesRead != -1){
                        tmp += new String(buf, 0, 1);
                        cont++;
                    }
                    else {
                        return null;
                    }

                }

                Log.d(LOGCostants.LOG_D_TAG, "TMP " + tmp);

                /*Reading the rest of the string*/
                if (tmp != null && tmp.length() == cmdChar) {

                    if (tmp.equals(MessageUtilities.MOD_TX) || tmp.equals(MessageUtilities.MOD_RX)) {
                        msg = readChatMessages(tmp);
                    } else if (tmp.equals(MessageUtilities.KEEP)) {
                        return tmp;
                    } else {
                        msg = readServerCommand(tmp);
                    }
                }

            } catch(SocketTimeoutException to) {
                Log.d(LOGCostants.LOG_D_TAG, "READ TIMEOUT REACHED");
                return null;
            } catch (IOException e) {
                Log.e(LOGCostants.LOG_E_TAG, "Error receiving a message from the server");
                return null;
            }
        }
        if (msg != null && !msg.equals("")) {
            Log.e(LOGCostants.LOG_D_TAG, "CMD = " + msg);
            tmp += msg;
        } else {
            return null;
        }
        return tmp;
    }


    /*Reading the rest of a TX_M RX_M message*/
    private String readChatMessages(String cmd) {

        int numOfPipes = getMessageParser().getNumOfPipes(cmd);
        int cont = 0;
        String msg = "";
        byte[] buf = new byte[1];
        int bytesRead = 0;

        /*reading until the number of pipes for the command is reached*/
        try {
            if (cmd != null) {
                do {
                    if ((bytesRead = rcvd.read(buf)) != 0 && bytesRead != -1 ) {
                        if ((char) buf[0] == '|') {
                            cont++;
                        }
                        msg += new String(buf, 0,  1);
                    }
                    else
                        return null;
                } while (cont < numOfPipes);



                /*We need to find the chat message length and read it*/
                int len = Integer.valueOf(MessageParser.messageParser(msg)[3]);
                cont = 0;
                byte[] encryptedMsg = new byte[len];
                while (cont < len) {
                    if ((bytesRead = rcvd.read(buf)) != 0 && bytesRead != -1 ) {
                        //msg += new String(buf, 0,  1);
                        encryptedMsg[cont] = buf[0];
                    }
                    else
                        return null;
                    cont++;
                }


                String dMsg = SecurityUtilities.decryptMessage("Trunksistor12345", encryptedMsg);
                if (dMsg != null)
                    return (msg += dMsg);
                else
                    return null;

            }
        } catch (IOException e) {
            Log.e(LOGCostants.LOG_E_TAG, "Error receiving a message from the server - readChatMessages");
            return null;
        }

        return msg;
    }

    /*Reading the rest of a command string from the server*/
    private String readServerCommand(String cmd) {

        int numOfPipes = getMessageParser().getNumOfPipes(cmd);
        int cont = 0;
        String msg = "";
        byte[] buf = new byte[1];
        int bytesRead = 0;

         /*reading until the number of pipes for the command is reached*/
        try {
            if (cmd != null) {
                do {
                    if ((bytesRead = rcvd.read(buf)) != 0 && bytesRead != -1 ) {
                        if ((char) buf[0] == '|') {
                            cont++;
                        }
                        msg += new String(buf, 0,  1);
                    }
                    else
                        return null;
                } while (cont < numOfPipes);
            }
        } catch (IOException e) {
            Log.e(LOGCostants.LOG_E_TAG, "Error receiving a message from the server - readServerCommand");
            return null;
        }
        return msg;

    }

    public boolean getSignedIn() {
        boolean result;
        slock.lock();
        result = signedIn;
        slock.unlock();
        return result;
    }

    public void setSignedIn(boolean value) {
        slock.lock();
        signedIn = value;
        slock.unlock();
    }

}
