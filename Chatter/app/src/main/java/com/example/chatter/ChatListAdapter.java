package com.example.chatter;

import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.chatter.chatModel.ChatMessageModel;
import com.example.chatter.chatModel.MessageStatus;
import com.example.chatter.chatModel.UserType;
import com.example.chatter.widget.Emoji;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.List;

public class ChatListAdapter extends BaseAdapter {

    private List<ChatMessageModel> chatMessageList;
    private Context context;
    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("h:mm a", Locale.ITALY);

    public ChatListAdapter(ArrayList<ChatMessageModel> cml, Context context) {
        chatMessageList = cml;
        this.context = context;
    }

    @Override
    public int getCount() {
        return chatMessageList.size();
    }

    @Override
    public Object getItem(int i) {
        return chatMessageList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = null;
        ViewHolderOTHER  viewHolderOTHER;
        ViewHolderSELF viewHolderSELF;
        ChatMessageModel msg = chatMessageList.get(i);

        if (msg.getUserType() == UserType.SELF) {
            v = LayoutInflater.from(context).inflate(R.layout.chat_self_item, null, false);
            viewHolderSELF = new ViewHolderSELF();
            viewHolderSELF.messageTextView = (TextView) v.findViewById(R.id.textview_message);
            viewHolderSELF.timeTextView = (TextView) v.findViewById(R.id.textview_time);
            v.setTag(viewHolderSELF);
            viewHolderSELF.messageTextView.setText(Html.fromHtml(Emoji.replaceEmoji(msg.getMessage(),
                    viewHolderSELF.messageTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16))
                    + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
            viewHolderSELF.timeTextView.setText(SIMPLE_DATE_FORMAT.format(msg.getMessageTime()));
        }

        else if (msg.getUserType() == UserType.OTHER) {
            v = LayoutInflater.from(context).inflate(R.layout.chat_other_item, null, false);
            viewHolderOTHER = new ViewHolderOTHER();
            viewHolderOTHER.messageTextView = (TextView) v.findViewById(R.id.textview_message);
            viewHolderOTHER.timeTextView = (TextView) v.findViewById(R.id.textview_time);
            viewHolderOTHER.messageStatus = (ImageView) v.findViewById(R.id.user_reply_status);
            v.setTag(viewHolderOTHER);
            viewHolderOTHER.messageTextView.setText(Html.fromHtml(Emoji.replaceEmoji(msg.getMessage(),
                    viewHolderOTHER.messageTextView.getPaint().getFontMetricsInt(), AndroidUtilities.dp(16))
                    + " &#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;" +
                    "&#160;&#160;&#160;&#160;&#160;&#160;&#160;"));
            viewHolderOTHER.timeTextView.setText(SIMPLE_DATE_FORMAT.format(msg.getMessageTime()));

            if (msg.getMessageStatus() == MessageStatus.DELIVERED) {
                viewHolderOTHER.messageStatus.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.message_got_receipt_from_target, null));
            }

            else if (msg.getMessageStatus() == MessageStatus.SENT) {
                viewHolderOTHER.messageStatus.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.message_got_receipt_from_server, null));
            }
            else if (msg.getMessageStatus() == MessageStatus.NOTSENT) {
                viewHolderOTHER.messageStatus.setImageDrawable(ResourcesCompat.getDrawable(context.getResources(), R.drawable.ic_schedule_black_24dp, null));
            }
        }

        else {
            Log.e(LOGCostants.LOG_E_TAG, "ERROR: UserType Undefined");
        }

        return v;
    }




    /*Used to set message ballon*/
    private class ViewHolderOTHER {
        public TextView messageTextView;
        public TextView timeTextView;
        public ImageView messageStatus;    }

    private class ViewHolderSELF {
        public TextView messageTextView;
        public TextView timeTextView;
    }
}
