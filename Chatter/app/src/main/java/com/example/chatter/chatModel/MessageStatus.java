package com.example.chatter.chatModel;

public enum MessageStatus {
    NOTSENT, SENT, DELIVERED, READ
};
