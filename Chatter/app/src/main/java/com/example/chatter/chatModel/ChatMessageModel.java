package com.example.chatter.chatModel;

public class ChatMessageModel {

    private String msg;
    private UserType type;
    private MessageStatus status;
    private long time;

    public ChatMessageModel(String m, UserType t, MessageStatus status) {
        msg = m;
        type = t;
        this.status = status;
    }

    public void setMessageStatus(MessageStatus status) {
        this.status = status;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public UserType getUserType(){
        return type;
    }

    public String getMessage(){
        return msg;
    }

    public long getMessageTime() {
        return time;
    }

    public MessageStatus getMessageStatus() {
        return status;
    }

}
