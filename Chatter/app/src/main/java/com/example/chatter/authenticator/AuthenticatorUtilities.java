package com.example.chatter.authenticator;


public class AuthenticatorUtilities {
    protected static final String ARG_ACCOUNT_NAME = "ARG_ACCOUNT_NAME";
    protected static final String ARG_ACCOUNT_TYPE = "ARG_ACCOUNT_TYPE";
    protected static final String ARG_ACCOUNT_PSWD = "ARG_ACCOUNT_PSWD";
    protected static final String ARG_AUTH_TYPE = "ARG_AUTH_TYPE";
    protected static final String KEY_ACCOUNT_AUTHENTICATOR_RESPONSE = "KEY_ACCOUNT_AUTHENTICATOR_RESPONSE";
    protected static final String ARG_BOOL_ADDING_NEW_ACCOUNT = "ARG_BOOL_ADDING_NEW_ACCOUNT";

    protected static final String AUTHTOKEN_TYPE_FULL_ACCESS = "Full access";

    public static final ChatterServerAuthenticate serverAuthenticate = new ChatterServerAuthenticate();
}
