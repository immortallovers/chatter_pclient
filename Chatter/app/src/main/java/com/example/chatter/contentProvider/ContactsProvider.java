package com.example.chatter.contentProvider;

import android.annotation.SuppressLint;
import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.util.Log;

import com.example.chatter.LOGCostants;
import com.example.chatter.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static android.content.Intent.ACTION_EDIT;

/*Used to retrieve and modify the device's contacts list*/
public class ContactsProvider extends  ContentProvider {

    private static Uri CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
    private static String DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME_PRIMARY;
    private static Uri CONTENT_URI_DATA = ContactsContract.Data.CONTENT_URI;
    private static Uri CONTENT_URI_RAW = ContactsContract.RawContacts.CONTENT_URI;
    //Contains the columns we want to return from the query to list all contacts
    @SuppressLint("InlinedApi")
    private static final String[] PROJECTION_NAME_LIST = {
            Build.VERSION.SDK_INT
                    >= Build.VERSION_CODES.HONEYCOMB ?
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                    ContactsContract.Contacts.DISPLAY_NAME
    };


    /*
     * Always return true, indicating that the
     * provider loaded correctly.
     */
    @Override
    public boolean onCreate() {
        return true;
    }

    /*
     * Return type for MIME type
     */
    @Override
    public String getType(Uri uri) {
        return null;
    }

    /*
     * query() always returns no results
     *
     */
    @Override
    public Cursor query(
            Uri uri,
            String[] projection,
            String selection,
            String[] selectionArgs,
            String sortOrder) {
        return null;
    }

    /*
     * insert() always returns null (no URI)
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        return null;
    }

    /*
     * delete() always returns "no rows affected" (0)
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        return 0;
    }

    /*
     * update() always returns "no rows affected" (0)
     */
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        return 0;
    }


    /*Fill the listOfContacts with the names of existing Contacts*/
    public static void populateConctactsListView(List<String> listOfContacts, Context context) {
        ContentResolver resolver = context.getContentResolver();
        Cursor cursor = resolver.query(CONTENT_URI, PROJECTION_NAME_LIST, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                String name = cursor.getString(cursor.getColumnIndex(DISPLAY_NAME));
                if (name != null) {
                    listOfContacts.add(name);
                }
            } while (cursor.moveToNext());
        }

        Collections.sort(listOfContacts);
        cursor.close();
    }


    /*Insert a new RawContact for the contact name with their Chatter userName*/
    public static void insertRawContact(String name, String username, Context context) {
        ContentResolver resolver = context.getContentResolver();
        ArrayList<ContentProviderOperation> ops = new ArrayList<ContentProviderOperation>();


        int rawContactInsertIndex = ops.size();
        ops.add(ContentProviderOperation.newInsert(CONTENT_URI_RAW)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, context.getString(R.string.accountType))
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, context.getString(R.string.app_name))
                .withValue(ContactsContract.RawContacts.AGGREGATION_MODE, ContactsContract.RawContacts.AGGREGATION_MODE_DEFAULT)
                .withValue(ContactsContract.RawContacts.SYNC1, "prova")
                .build());

        ops.add(ContentProviderOperation.newInsert(CONTENT_URI_DATA)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name)
                .build());

        ops.add(ContentProviderOperation
                .newInsert(CONTENT_URI_DATA)
                .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, rawContactInsertIndex)
                .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE)
                .withValue(ContactsContract.CommonDataKinds.Im.DATA, username)
                .withValue(ContactsContract.CommonDataKinds.Im.PROTOCOL, ContactsContract.CommonDataKinds.Im.PROTOCOL_CUSTOM)
                .withValue(ContactsContract.CommonDataKinds.Im.CUSTOM_PROTOCOL, context.getString(R.string.app_name))
                .build());

        try {
            resolver.applyBatch(ContactsContract.AUTHORITY, ops);
        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (OperationApplicationException e) {
            e.printStackTrace();
        }
    }

    /*Returns a string with the memorized userName for the contact with DISPLAY_NAME name or a void string if there's no field Chatter in the contact*/
    public static ContactObject getUserName(String name, Context context) {
        ContactObject contact = new ContactObject();
        boolean namefound = false;
        Cursor tmp = null;

        ContentResolver resolver = context.getContentResolver();

        /*Getting the list of rawContacts created with the app account*/
        Cursor cursor = resolver.query(CONTENT_URI_RAW, new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.ACCOUNT_NAME + " = ? AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?",
                new String[]{context.getString(R.string.app_name), context.getString(R.string.accountType)}, null);

        Log.d(LOGCostants.LOG_D_TAG, "cursor = " + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts._ID));

                    /*Getting the list of contacts' names which belonged to rawContacts created by the app*/
                    tmp = resolver.query(CONTENT_URI_DATA, new String[]{ContactsContract.Data.DATA1},
                            ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                            new String[]{id, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);

                    Log.d(LOGCostants.LOG_D_TAG, "tmp = " + tmp.getCount());

                    if (tmp.getCount() > 0) {
                        if (tmp.moveToFirst()) {
                            do {
                                /*Looking for the rawContact whose name field equals name*/
                                if (tmp.getString(tmp.getColumnIndex(ContactsContract.Data.DATA1)).equals(name)) {
                                    /*We found our contact - the Data table field with the needed info will have the same rawContactID*/
                                    namefound = true;
                                }
                            } while (tmp.moveToNext() && !namefound);
                        }
                    }
                    if (namefound) {
                        /*Getting tha Chatter username for Contact name*/
                        tmp = resolver.query(CONTENT_URI_DATA, new String[]{ContactsContract.Data.DATA1},
                                ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                                new String[]{id, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE}, null);
                        if (tmp.getCount() > 0) {
                            if (tmp.moveToFirst()) {
                                contact.setUserName(tmp.getString(tmp.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA)));
                                contact.setRawContactID(id);
                            }
                        }
                    }

                    Log.d(LOGCostants.LOG_D_TAG, "userName = " + contact.getUserName());
                    if (contact.getUserName() != null && namefound) {
                        break;
                    }
                } while (cursor.moveToNext());
            }
        }

        if (tmp != null) {
            tmp.close();
        }
        cursor.close();
        return contact;
    }


    /*Returns the list of Chatter contacts*/
    public static List<String> getUsernameList(Context context) {
        List<String> list = new ArrayList<String>();
        Cursor usernames = null;

        ContentResolver resolver = context.getContentResolver();

        /*Getting the list of rawContacts created with the app account*/
        Cursor cursor = resolver.query(CONTENT_URI_RAW, new String[]{ContactsContract.RawContacts._ID},
                ContactsContract.RawContacts.ACCOUNT_NAME + " = ? AND " + ContactsContract.RawContacts.ACCOUNT_TYPE + " = ?",
                new String[]{context.getString(R.string.app_name), context.getString(R.string.accountType)}, null);

        Log.d(LOGCostants.LOG_D_TAG, "cursor = " + cursor.getCount());

        if (cursor.getCount() > 0) {
            if (cursor.moveToFirst()) {
                do {
                    String id = cursor.getString(cursor.getColumnIndex(ContactsContract.RawContacts._ID));

                    /*Getting the list of contacts' names which belonged to rawContacts created by the app*/
                    usernames = resolver.query(CONTENT_URI_DATA, new String[]{ContactsContract.Data.DATA1},
                            ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                            new String[]{id, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE}, null);

                    if (usernames.getCount() > 0) {
                        if (usernames.moveToFirst()) {
                            do {
                                Cursor tmp = resolver.query(CONTENT_URI_DATA, new String[]{ContactsContract.Data.DATA1},
                                        ContactsContract.Data.RAW_CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?",
                                        new String[]{id, ContactsContract.CommonDataKinds.Im.CONTENT_ITEM_TYPE}, null);
                                if (tmp.getCount() > 0) {
                                    if (tmp.moveToFirst()) {
                                        list.add(tmp.getString(tmp.getColumnIndex(ContactsContract.CommonDataKinds.Im.DATA)));
                                    }
                                }
                            } while (usernames.moveToNext());
                        }
                    }

                } while (cursor.moveToNext());
            }
        }
        return list;
    }
}
