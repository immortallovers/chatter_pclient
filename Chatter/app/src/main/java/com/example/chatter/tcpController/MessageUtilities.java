package com.example.chatter.tcpController;

public class MessageUtilities {

    /*FIRST SIGN IN MSG --- C_ID|USERNAME|SHA256_PSW(128bit)*/
    public static String C_ID = "C_ID";
    //Server's answers ---- C_ID|ANSW|
    public static String C_ID_OK_ANSW = "C_ID|OK|";
    //Username already taken
    public static String C_ID_NO_ANSW = "C_ID|NO|";

    /*AUTHENTICATION --- AUTH|USERNAME|SHA256_PSW(128bit)*/
    public static String AUTH = "AUTH";
    //Server's answers --- AUTH|ANSW|
    public static String AUTH_OK_ANSW = "AUTH|OK|";
    //Wrong username or psw
    public static String AUTH_NO_ANSW = "AUTH|NO|";

    /*CREATION OF A NEW SINGLE/GROUP CHAT --- CNTC|USERNAME_DEST| or CNTC|GROUP_NAME|*/
    public static String CNTC = "CNTC";
    //Server's answers --- CNTC|ANSW|USERNAME_DEST| or CNTC|ANSW|GROUP_NAME|
    public static String CNTC_OK_ANSW = "CNTC|OK";
    //Group chat's name already taken
    public static String CNTC_NO_ANSW = "CNTC|NO";

    /*SENDING A MESSAGE --- USERNAME_DEST|MSG*/
    //Server's answer ---  TX_X|USERNAME_DEST|dd:MM:yy:hh:mm::ss|MSG_LEN|MSG|
    public static String MOD_TX = "TX_M";

    /*RECEIVENG A MESSAGE --- RX_X|USERNAME_MITT|dd:MM:yy:hh:mm::ss|MSG_LEN|MSG|*/
    public static String MOD_RX= "RX_M";

    /*GROUP CREATION*/
    public static String GRPC = "GRPC";
    public static String GRPC_NW_ANSW = "GRPC|NW";
    public static String GRPC_NO_ANSW = "GRPC|NO";

    /*ADDING USER TO A GROUP*/
    public static String GRPU = "GRPU";
    public static String GRPU_OK_ANSW = "GRPU|OK";
    public static String GRPU_NO_ANSW = "GRPU|NO";

    /*THE USER HAS BEEN ADDED TO A NEW GROUP*/
    public static String GRPA = "GRPA";

    /*SENDING A MEDIA FILE TO THE SERVER*/
    public static String FILE = "FILE";
    //Server's answer --- "FILE|OK|chat|fileName or "FILE|NO|chat|fileName"
    public static String FILW_OK_ANSW = "FILE|OK";
    public static String FILW_NO_ANSW = "FILE|NO";

    /*RECEIVING THE URL TO DOWNLOAD A NEW MEDIA FILE FROM SERVER*/
    public static String NFLE = "NFLE";

    /*RECEIVING KEEPALIVE FROM SERVER*/
    public static String KEEP = "KEEP";

}
