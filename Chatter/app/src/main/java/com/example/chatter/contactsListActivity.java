package com.example.chatter;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.chatter.chatModel.UserType;
import com.example.chatter.contentProvider.ContactObject;
import com.example.chatter.contentProvider.ContactsProvider;
import com.example.chatter.parser.MessageParser;
import com.example.chatter.tcpController.MessageUtilities;
import com.example.chatter.tcpController.TCPClient;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

public class contactsListActivity extends AppCompatActivity {

    private ListView contacts_list;
    private List<String> listOfContacts;
    private static Context context;
    private static EditText userName;
    private static final String EXTRA_NAME = "com.example.chatter.EXTRA_NAME";
    private static final String EXTRA_USERNAME = "com.example.chatter.EXTRA_USERNAME";
    private String nickName;
    private String contactName;
    private ContactReceiver receiver;


    /*Checks if the userName contains special chars*/
    private boolean containsSpecialChar(String name) {
        if (name.contains("_") || name.contains("|"))
            return true;
        return false;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contactslist);


        context = this;

        receiver = new ContactReceiver();

        /*CONTACTS LIST*/
        contacts_list = (ListView) findViewById(R.id.contacts_list);
        listOfContacts = new ArrayList<String>();
        ContactsProvider.populateConctactsListView(listOfContacts, this);
        final ArrayAdapter<String> conctactsListAdapter = new ArrayAdapter<String>(this, R.layout.listview_layout, android.R.id.text1, listOfContacts);
        contacts_list.setAdapter(conctactsListAdapter);
        contacts_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                contactName = conctactsListAdapter.getItem(i).toString();
                final ContactObject contact;
                contact = ContactsProvider.getUserName(contactName, context);
                nickName = contact.getUserName();

                /*If there is no username for the selected contact, ask the user to add one*/
                if (nickName == null) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(contactsListActivity.this);
                    View v = contactsListActivity.this.getLayoutInflater().inflate(R.layout.username_dialog_layout, null);
                    builder.setView(v);
                    userName = (EditText) v.findViewById(R.id.dialogUsername);
                    builder.setTitle(getString(R.string.dialogTitle))
                            .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.cancel();
                                }
                            })
                            .setPositiveButton("INSERT", new DialogInterface.OnClickListener() {

                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String uName = userName.getText().toString();
                                    if (!uName.equals("") && uName.length()>4 && !containsSpecialChar(uName)) {

                                        uName = uName.toLowerCase();
                                        String send = MessageUtilities.CNTC + "|" + uName + "|";
                                         /*Getting the byte[] to send*/
                                        byte[] toBytes = send.getBytes(Charset.forName("UTF-8"));

                                        if (!TCPClient.getTCPClient().sendMsg(toBytes)) {
                                            Toast.makeText(context, R.string.serverUnreachable, Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            nickName = uName;
                                        }

                                    }
                                    else {
                                        userName.setText(R.string.dialogUsername);
                                        Toast.makeText(context, R.string.insertValidUsername, Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                    AlertDialog dialog = builder.create();
                    dialog.show();
                }
                else {
                    /* Creating a new Activity with the chat related to the selected user*/
                    Intent intent = new Intent(getApplicationContext(), chatActivity.class);
                    intent.putExtra(EXTRA_USERNAME, nickName);
                    startActivity(intent);
                    contactsListActivity.this.finish();
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
         /*
        * Registering the ContactReceiver
        * onResume is called after onCreate and when recovering from onPause
        * */
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, new IntentFilter(getString(R.string.contactReceiver)));
    }


    @Override
    protected void onPause() {
        super.onPause();
        /*Unregister the ContactReceiver if the activity is not running*/
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    /*Custom BroadcastReceiver to read messages from BackgroundService when the activity is running*/
    public class ContactReceiver extends BroadcastReceiver {

        private final String MESSAGE_TO_CONTACTLIST = "CNTC_ANSW";

        @Override
        public void onReceive(Context context, Intent intent) {
            String msg = intent.getStringExtra(MESSAGE_TO_CONTACTLIST);
            String correctAnswer = MessageUtilities.CNTC_OK_ANSW + "|" + nickName + "|";
            if (msg.equals(correctAnswer) && nickName != null) {
                 /*Creating a new RawContent for the selected user with username uName*/
                ContactsProvider.insertRawContact(contactName, nickName,  context);
                /* Creating a new Activity with the chat related to the selected user*/
                Intent i = new Intent(getApplicationContext(), chatActivity.class);
                i.putExtra(EXTRA_USERNAME, nickName);
                startActivity(i);
                contactsListActivity.this.finish();
            }
            else {
                Toast.makeText(context, R.string.noSuchContact, Toast.LENGTH_SHORT).show();
            }
        }
    }
}
